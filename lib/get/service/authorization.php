<?php

require_once "../lib/get/db/users.php";
require_once "../lib/facebook/autoload.php";


class get_service_Authorization {

  public function authenticateUser ($user) {
    $users      = new get_db_Users();
    $userToken  = $users->getUserAccessForToken($user->serviceToken);

    if(isset($userToken['token']) && $user->serviceToken == $userToken['token']) {
      return $userToken;
    }

    return false;

  }

  public function generateToken ($user) {
    if($this->authenticateUserAtFacebook($user->fbToken)) {
      $token     = "@get".bin2hex(random_bytes(77));
      $users     = new get_db_Users();
      $newUserId = $users->save($user);

      if($newUserId) {
        error_log("Created new user: " . json_encode($newUserId));
        $users->saveTokenForUser($newUserId, $token, $user->fbToken);
        return ["serviceToken" => $token,
                "id"           => $newUserId,
                "companyId"    => null];
      }else{
        $existingUser = $users->fetch($user->fbId);
        error_log("Existing user logged in!");

        $token = $users->getUserAccessForUser($existingUser['id']);
        error_log(json_encode($token));

        if(isset($token['user_id'])) {
          return ["serviceToken" => $token['token'],
                  "id"           => $token['user_id'],
                  "companyId"    => isset($token['companyId']) ? $token['companyId'] : null,
                  "companyName"  => isset($token['companyName']) ? $token['companyName'] : null];
        }
      }
      
      return ["Error" => "This is unexpected .. Seemingly an existing user but with no serviceToken"];
    }else {
      error_log("Facebook token invalid!");
      return ["error" => "Facebook token invalid!"];
    }
  }

  private function authenticateUserAtFacebook($fbToken){
    $fb = new Facebook\Facebook([
      'app_id' => '158921457926604',
      'app_secret' => '4969a412f05434f28f526c7041d64909',
      'default_graph_version' => 'v2.8',
    ]);

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,first_name', $fbToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      error_log($e);
      return false;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      error_log($e);
      return false;
    }

    $user = $response->getGraphUser();

    if($user['id']) {
      return true;
    }else {
      return false;
    }
  }

}
