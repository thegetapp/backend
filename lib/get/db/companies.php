<?php

require_once "../lib/get/db/db.php";

class get_db_Companies extends get_DB {

  const links = [
    "facebook"    => "https://s3.eu-central-1.amazonaws.com/thegetapp/assets/social/facebook.png",
    "instagram"   => "https://s3.eu-central-1.amazonaws.com/thegetapp/assets/social/instagram.png",
    "justeat"     => "https://s3.eu-central-1.amazonaws.com/thegetapp/assets/social/justEat.png",
    "pinterest"   => "https://s3.eu-central-1.amazonaws.com/thegetapp/assets/social/pinterest.png",
    "tripadvisor" => "https://s3.eu-central-1.amazonaws.com/thegetapp/assets/social/tripadvisor.png",
    "yelp"        => "https://s3.eu-central-1.amazonaws.com/thegetapp/assets/social/yelp.png",
    "foursquare"  => "https://s3.eu-central-1.amazonaws.com/thegetapp/assets/social/foursquare.png",
    "hungry"      => "https://s3.eu-central-1.amazonaws.com/thegetapp/assets/social/hungry.png"
  ];
  
  public function save($data, $userId) {
    if(true) { // TODO: Find a way to make sure we don't make duplicate companies.
      $companyId = $this->persist($data, $userId);
      $this->saveCompanyUser($companyId, $userId);

      if(isset($data->hours)) {
        $this->saveOpeningHours($data->hours, $companyId);
      }

      if(isset($data->links)) {
        if(isset($data->links->facebook)) {
          $this->saveCompanyLink($companyId, "facebook", $data->links->facebook);
        }
        if(isset($data->links->instagram)) {
          $this->saveCompanyLink($companyId, "instagram", $data->links->instagram);
        }
        if(isset($data->links->justeat)) {
          $this->saveCompanyLink($companyId, "justeat", $data->links->justeat);
        }
        if(isset($data->links->pinterest)) {
          $this->saveCompanyLink($companyId, "pinterest", $data->links->pinterest);
        }
        if(isset($data->links->tripadvisor)) {
          $this->saveCompanyLink($companyId, "tripadvisor", $data->links->tripadvisor);
        }
        if(isset($data->links->yelp)) {
          $this->saveCompanyLink($companyId, "yelp", $data->links->yelp);
        }
        if(isset($data->links->foursquare)) {
          $this->saveCompanyLink($companyId, "foursquare", $data->links->foursquare);
        }
        if(isset($data->links->hungry)) {
          $this->saveCompanyLink($companyId, "hungry", $data->links->hungry);
        }
      }

      return ["company_id" => $companyId, "company_name" => $data->title];
    }
    return ["ok" => "Company already exist"];
  }

  public function fetch($company){
    $stmt = "SELECT id FROM company WHERE id = {$company->id}";
    $result = parent::query($stmt);
    $result = $result->fetch(PDO::FETCH_ASSOC);

    if(isset($result['id'])) {
      return true;
    }
    return false;
  }

  public function fetchAllInfo($id){
    $stmt = "
        SELECT *
          FROM company c
     LEFT JOIN company_links cl
            ON cl.company_link = c.id
         WHERE c.id = :id
      ";
    $values = [":id" => $id];
    $company = parent::pQuery($stmt, $values);
    if($company) {
      $stmt = "
          SELECT *
            FROM assortment
           WHERE company_id = :id";
      $values = [":id" => $id];
      $assortments = parent::pQuery($stmt, $values);

      $company['assortments'] = $assortments;
    }
    return $company;
  }

  public function getCompanyByGooglePlaceId($pId){
    $stmt = "SELECT id FROM company WHERE google_place_id = :pId";
    $values = [
      ":pId" => $pId
    ];

    $result = parent::pQuery($stmt, $values);
    return $result;
  }

  public function saveReviewerCompany($company, $userId){
    $stmt = "INSERT IGNORE INTO company 
                          (
                            user_id,
                            google_place_id,
                            title,
                            street,
                            lat,
                            lng,
                            country,
                            city,
                            zip,
                            created_dtm
                          )
                   VALUES (
                            :user_id,
                            :google_place_id,
                            :title,
                            :street,
                            :lat,
                            :lng,
                            :country,
                            :city,
                            :zip,
                            now()
                          )";
    
    $values =[
      ":user_id"         => $userId,
      ":google_place_id" => $company->google_place_id,
      ":street"          => $company->street,
      ":lat"             => $company->lat,
      ":lng"             => $company->lng,
      ":title"           => $company->company_title,
      ":country"         => $company->country,
      ":city"            => $company->city,
      ":zip"             => $company->zip,
    ]; 

    parent::insert($stmt, $values);
    return parent::getLastId();
  }

  private function persist ($company, $userId) {
    $stmt = "INSERT INTO company
                          (
                           user_id,
                           facebook_id,
                           google_place_id,
                           street,
                           country,
                           city,
                           zip,
                           phone,
                           website,
                           title,
                           description,
                           lng,
                           lat,
                           created_dtm
                          )
                   VALUES (
                           :user_id,
                           :facebook_id,
                           :google_place_id,
                           :street,
                           :country,
                           :city,
                           :zip,
                           :phone,
                           :website,
                           :title,
                           :description,
                           :lng,
                           :lat,
                           now()
                        )";
    $values = [
      ":user_id"         => $userId,
      ":facebook_id"     => isset($company->facebook_id) ? $company->facebook_id : null,
      ":google_place_id" => $company->google_place_id,
      ":street"          => $company->street,
      ":country"         => $company->country,
      ":city"            => $company->city,
      ":zip"             => $company->zip,
      ":phone"           => $company->phone,
      ":website"         => $company->website,
      ":title"           => $company->title,
      ':description'     => $company->description,
      ":lng"             => $company->lng,
      ":lat"             => $company->lat,
    ];

    parent::insert($stmt, $values);
    return parent::getLastId();
  }

  public function updateExistingCompany($data, $userId){
    // Update reviewer company -> official company
    $this->updateCompany($data, $userId);

    // Add administrator to company
    $company   = $this->getCompanyByGooglePlaceId($data->google_place_id);
    $companyId = $company['id'];
    $this->saveCompanyUser($companyId, $userId);

    if(isset($data->hours)) {
      $this->saveOpeningHours($data->hours, $companyId);
    }

    if(isset($data->links)) {
      if(isset($data->links->facebook)) {
        $this->saveCompanyLink($companyId, "facebook", $data->links->facebook);
      }
      if(isset($data->links->instagram)) {
        $this->saveCompanyLink($companyId, "instagram", $data->links->instagram);
      }
      if(isset($data->links->justeat)) {
        $this->saveCompanyLink($companyId, "justeat", $data->links->justeat);
      }
      if(isset($data->links->pinterest)) {
        $this->saveCompanyLink($companyId, "pinterest", $data->links->pinterest);
      }
      if(isset($data->links->tripadvisor)) {
        $this->saveCompanyLink($companyId, "tripadvisor", $data->links->tripadvisor);
      }
      if(isset($data->links->yelp)) {
        $this->saveCompanyLink($companyId, "yelp", $data->links->yelp);
      }
      if(isset($data->links->foursquare)) {
        $this->saveCompanyLink($companyId, "foursquare", $data->links->foursquare);
      }
      if(isset($data->links->hungry)) {
        $this->saveCompanyLink($companyId, "hungry", $data->links->hungry);
      }
    }

    return ["company_id" => $companyId, "company_name" => $data->title];
  }

  public function updateCompany($company, $userId){
        $stmt = "UPDATE company
                    SET user_id     = :user_id,
                        facebook_id = :facebook_id,
                        phone       = :phone,
                        website     = :website,
                        title       = :title,
                        description = :description
                  WHERE google_place_id = :google_place_id 
                        ";
    $values = [
      ":user_id"         => $userId,
      ":facebook_id"     => isset($company->facebook_id) ? $company->facebook_id : null,
      ":phone"           => $company->phone,
      ":website"         => $company->website,
      ":title"           => $company->title,
      ':description'     => $company->description,
      ":google_place_id" => $company->google_place_id
    ];

    parent::insert($stmt, $values);
    return parent::getLastId();
  }

  private function saveCompanyUser($companyId, $userId) {
    $stmt = "INSERT INTO company_user (user_id, company_id) VALUES ({$userId}, {$companyId})";
    parent::query($stmt);
    return parent::getLastId();
  }

  private function saveCompanyLink($cId, $network, $path){
    if(isset(self::links[$network]) && $path != "") {
      
      $httpPos = strpos($path, "http");
      if($httpPos === false) {
        $path = "http://" . $path;
      }

      $stmt = "INSERT INTO company_links (company_id, network, path, logo_path)
               VALUES (:company_id, :network, :path, :logo_path)";
      $values = [
        ":company_id" => $cId, 
        ":network"    => $network,
        ":path"       => $path,
        ":logo_path"  => self::links[$network]
      ];

      parent::insert($stmt, $values);
    }
  }

  private function saveOpeningHours ($days, $companyId) {
    if(isset($days->mon_1_open)) {
      if(isset($days->mon_1_close)) {
        $this->persistOpeningHours("Mandag", $days->mon_1_open, $days->mon_1_close, $companyId);
      }else {
        $this->persistOpeningHours("Mandag", $days->mon_1_open, null, $companyId);
      }
    }
    if(isset($days->tue_1_open)) {
      if(isset($days->tue_1_close)) {
        $this->persistOpeningHours("Tirsdag", $days->tue_1_open, $days->tue_1_close, $companyId);
      }else {
        $this->persistOpeningHours("Tirsdag", $days->tue_1_open, null, $companyId);
      }
    }
    if(isset($days->wed_1_open)) {
      if(isset($days->wed_1_close)) {
        $this->persistOpeningHours("Onsdag", $days->wed_1_open, $days->wed_1_close, $companyId);
      }else {
        $this->persistOpeningHours("Onsdag", $days->wed_1_open, null, $companyId);
      }
    }
    if(isset($days->thu_1_open)) {
      if(isset($days->thu_1_close)) {
        $this->persistOpeningHours("Torsdag", $days->thu_1_open, $days->thu_1_close, $companyId);
      }else {
        $this->persistOpeningHours("Torsdag", $days->thu_1_open, null, $companyId);
      }
    }
    if(isset($days->fri_1_open)) {
      if(isset($days->fri_1_close)) {
        $this->persistOpeningHours("Fredag", $days->fri_1_open, $days->fri_1_close, $companyId);
      }else {
        $this->persistOpeningHours("Fredag", $days->fri_1_open, null, $companyId);
      }
    }
    if(isset($days->sat_1_open)) {
      if(isset($days->sat_1_close)) {
        $this->persistOpeningHours("Lørdag", $days->sat_1_open, $days->sat_1_close, $companyId);
      }else {
        $this->persistOpeningHours("Lørdag", $days->sat_1_open, null, $company->id);
      }
    }
    if(isset($days->sun_1_open)) {
      if(isset($days->sun_1_close)) {
        $this->persistOpeningHours("Søndag", $days->sun_1_open, $days->sun_1_close, $companyId);
      }else {
        $this->persistOpeningHours("Søndag", $days->sun_1_open, null, $companyId);
      }
    }

  }

  private function persistOpeningHours($day, $open, $close, $companyId) {
    $stmt = "INSERT INTO opening_hours (company_id, day_of_week, open, close)
             VALUES (
                     :companyId,
                     :day,
                     :open,
                     :close
                    )
    ";

    $values = [
      ":companyId" => $companyId,
      ":day"       => $day,
      ":open"      => $open,
      ":close"     => $close
    ];

    parent::insert($stmt, $values);
  }

}
