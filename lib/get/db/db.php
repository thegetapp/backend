<?php

abstract class get_DB {

  private $servername, $username, $password, $dbname;
  public $conn;

  function __construct(){
    $this->servername = "46.101.123.234";
    $this->username   = "getservice";
    $this->password   = "y5HvfxaD";
    $this->dbname     = "getdev";

    // Create connection

    try {
      $this->conn = new PDO("mysql:host={$this->servername};port=3306;dbname=getdev", $this->username, $this->password);
      // set the PDO error mode to exception
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      echo "Connected successfully";
    }
    catch(PDOException $e)
    {
      error_log($e);
      var_dump($e);die();
      echo "Connection failed: " . $e->getMessage();
    }
  }

  public function query($stmt){
    $result = $this->conn->query($stmt);
    return $result;
  }

  public function pExecute($stmt, array $values){
    $statement = $this->conn->prepare($stmt);
    $statement->execute($values);
  }

  public function insert($stmt, array $values){
    $statement = $this->conn->prepare($stmt);
    $statement->execute($values);
  }

  public function pQuery($stmt, $values, $fetchType = "fetch"){
    $statement = $this->conn->prepare($stmt);
    $statement->execute($values);
    
    if($fetchType == "fetch") {
      $result = $statement->fetch(PDO::FETCH_ASSOC);

    }elseif($fetchType == "fetchAll") {
      $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    return $result;
  }

  public function getLastId(){
    $last_id = $this->conn->lastInsertid();
    return $last_id;
  }

}

 ?>
