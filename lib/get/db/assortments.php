<?php

require_once "../lib/get/db/db.php";

class get_db_Assortments extends get_DB {

  public function save($data, $userId, $companyId, $imgPath){
    $this->persist($data, $userId, $companyId, $imgPath);
  }

  public function fetch(array $data){
    foreach ($data as $object) {
      $lat  = $object->lat;
      $long = $object->long;
      if(isset($object->offset)) {
        $offset = $object->offset;
      }else {
        $offset = 1;
      }
      $recommendations = $this->getNearByAssortments($lat, $long, 15, $offset);
    }
    return $recommendations;
  }

  public function fetchHighlights(array $data){
    foreach ($data as $object) {
      $lat  = $object->lat;
      $long = $object->long;
      if(isset($object->offset)) {
        $offset = $object->offset;
      }else {
        $offset = 1;
      }
      $recommendations = $this->getNewest($lat, $long, 15, $offset);
    }
    return $recommendations;
  }

  public function persist($assortment, $userId, $companyId, $imgPath){

    if(strpos($assortment->price, ",")) {
      $assortment->price = str_replace(",", ".", $assortment->price);
    }

    if(isset($assortment->assortmentId)) {
      $this->hideAssortment($assortment->assortmentId, intval($userId));
    }

    $assortment->price = number_format(floatval($assortment->price), 2, '.', '');

    $stmt = "
      INSERT INTO assortment (
                               title,
                               company_id,
                               user_id,
                               price,
                               description,
                               img_path,
                               created_dtm,
                               user_type,
                               is_valid
                             )
           VALUES (
                   :title,
                   :company_id,
                   :user_id,
                   :price,
                   :description,
                   :img_path,
                   now(),
                   :user_type,
                   :is_valid
                  )
      ";

    $values = [
      ":title"       => $assortment->title_food,
      ":company_id"  => intval($companyId),
      ":user_id"     => intval($userId),
      ":price"       => $assortment->price,
      ":description" => $assortment->description_food,
      ":img_path"    => $imgPath,
      ":user_type"   => $assortment->usrType,
      ":is_valid"    => isset($assortment->is_valid) ? $assortment->is_valid : 1,
    ];

    parent::insert($stmt, $values);
  }

  private function hideAssortment($id, $userId){
    $stmt = "UPDATE assortment set visible = 0 where id = :id AND user_id = :user_id";
    $values = [":id" => $id, ":user_id" => $userId];
    parent::pExecute($stmt, $values);
  }

  private function getNearByAssortments($lat, $long, $distance = 15, $offset = 1, $order = "distance_accurate", $limit = 35){
    $stmt = "
      SELECT
            ( 6371 * acos( cos( radians({$lat}) ) * cos( radians( c.lat ) ) * cos( radians( c.lng ) - radians({$long}) ) + sin( radians({$lat}) ) * sin( radians( c.lat ) ) ) ) AS 'distance',
            round(( 6371 * acos( cos( radians({$lat}) ) * cos( radians( c.lat ) ) * cos( radians( c.lng ) - radians({$long}) ) + sin( radians({$lat}) ) * sin( radians( c.lat ) ) ) ), 2) AS 'distance_accurate',
             a.id as _id,
             c.id as company_id,
             c.google_place_id,
             4 as rating,
             a.title as title_food,
             a.description as description_food,
             REPLACE(price, '.', ',') as price,
             a.img_path,
             a.user_type as 'usrType',
             c.title as title_company,
             c.description as description_company,
             c.phone,
             c.street,
             c.street_number,
             c.zip,
             c.city,
             c.website,
             c.lat,
             c.lng,
             c.country,
             c.facebook_id,
             a.created_dtm,
             CONCAT('https://graph.facebook.com/v2.8/', c.facebook_id, '/picture') as logo_path
        FROM assortment a
        JOIN company c
          ON a.company_id = c.id
       WHERE a.is_valid = 1
         AND a.visible = 1
    ORDER BY {$order} ASC, a.id DESC, usrType
       LIMIT {$limit}
      OFFSET {$offset}";

    $result = parent::query($stmt);
    $result = $result->fetchAll(PDO::FETCH_ASSOC);

    if(count($result) < 10 && $distance < 60) {
      $distance = $distance + 15;
      $this->getNearByAssortments($lat, $long, $distance, $offset, $order);
    }
    $response = [];
    foreach ($result as $company) {
      $company['price_digit']    = explode(",", $company['price'])[0];
      $company['price_decimal']  = explode(",", $company['price'])[1] == "00" ? "" : explode(",", $company['price'])[1];
      $company['lat']            = $company['lat'] + 0;
      $company['lng']            = $company['lng'] + 0;
      $company['hours']          = $this->getOpeningHoursForCompany($company['company_id']);
      $company['related']        = $this->getAdditionalSuggestionsForSameCompany($company['company_id'], $company['_id']);
      $googleRelated             = $this->getAdditionalSuggestionsForSameCompanyByGooglePlaceId($company['_id'], $company['google_place_id']);

      if(count($googleRelated) > 0) {
        $company['related'] = array_merge($company['related'], $googleRelated);
        $related = array_values(array_unique($company['related'], SORT_REGULAR));
        $company['related'] = $related;
      }

      $company['links']          = $this->getSocialLinksForCompany($company['company_id']);

      if(floatval($company['distance']) < 1) {
        $floatDistance = floatval($company['distance']);
        $nearest50 = round(($floatDistance * 100) / 50) * 50;
        $company['distance'] = $nearest50;
        $company['distance_unit'] = "m";
      }else {
        $floatDistance = floatval($company['distance']);
        $company['distance'] = round($floatDistance, 1);
        $company['distance_unit'] = "km";
      }

      $response[] = $company;
    }

    return $response;
  }

  private function getAdditionalSuggestionsForSameCompany($cId, $aId){
    $stmt = "
      SELECT title as title_food,
             company_id,
             id as _id,
             description as description_food,
             REPLACE(price, '.', ',') as price,
             img_path,
             user_type as 'usrType',
             1 as 'related'
        FROM assortment a
       WHERE company_id = {$cId}
         AND a.id != {$aId}
         AND a.is_valid = 1
         AND a.visible = 1
       LIMIT 10";
    $result = parent::query($stmt);
    $result = $result->fetchAll(PDO::FETCH_ASSOC);

    $response = [];

    foreach ($result as $company) {
      $company['price_digit']    = explode(",", $company['price'])[0];;
      $company['price_decimal']  = explode(",", $company['price'])[1] == "00" ? "" : explode(",", $company['price'])[1];
      $response[] = $company;
    }
    return $response;
  }

  private function getAdditionalSuggestionsForSameCompanyByGooglePlaceId($aId, $gId){

    $stmt = "
      SELECT a.title as title_food,
             c.id as company_id,
             a.id as _id,
             a.description as description_food,
             REPLACE(price, '.', ',') as price,
             img_path,
             user_type as 'usrType',
             1 as 'related'
        FROM assortment a
        JOIN company c
          ON c.id = a.company_id
       WHERE a.id != :aId
         AND c.google_place_id = :google_place_id
         AND a.visible = 1
         AND a.is_valid = 1
    ";

    $values = [
      ":google_place_id" => $gId,
      ":aId"             => $aId
    ];

    $result = parent::pQuery($stmt, $values, "fetchAll");

    $response = [];
    foreach ($result as $company) {
      $company['price_digit']    = explode(",", $company['price'])[0];;
      $company['price_decimal']  = explode(",", $company['price'])[1] == "00" ? "" : explode(",", $company['price'])[1];
      $response[] = $company;
    }

    if($result) {
      return $response;
    }else {
      return [];
    }
  }


  private function getSocialLinksForCompany($cId) {
    $stmt = "SELECT network, path, logo_path from company_links where company_id = :cId";
    $values = [
      ":cId" => $cId
    ];

    $result = parent::pQuery($stmt, $values, "fetchAll");
    return $result;
  }

  private function getOpeningHoursForCompany($cId){
    $stmt = "SELECT day_of_week, open, close from opening_hours where company_id = :cId";
    $values = [
      ":cId" => $cId
    ];

    $result = parent::pQuery($stmt, $values, "fetchAll");
    return $result;
  }

  public function fetchAllByUserId($userId){
    $stmt = "SELECT max(a.id) as id,
                    a.title as title_food,
                    a.id as assortmentId,
                    a.description as description_food,
                    a.company_id,
                    c.title as title_company,
                    c.google_place_id,
                    c.facebook_id,
                    c.street,
                    c.city,
                    c.zip,
                    c.country,
                    c.lat,
                    c.lng,
                    a.img_path,
                    REPLACE(a.price, '.', ',') as price,
                    a.id as _id,
                    a.is_valid,
                    a.user_type as usrType
               FROM assortment a
               JOIN company c
                 ON a.company_id = c.id
              WHERE a.user_id = :user_id
                AND a.visible = 1
                AND a.id in (SELECT max(id) from assortment aa where aa.user_id = :user_id group by aa.img_path)
            GROUP BY a.img_path
           ORDER BY a.id DESC";
    $values = [":user_id" => $userId];

    $result = parent::pQuery($stmt, $values, "fetchAll");

    $response = [];
    foreach ($result as $company) {
      $company['price_digit']    = explode(",", $company['price'])[0];;
      $company['price_decimal']  = explode(",", $company['price'])[1] == "00" ? "" : explode(",", $company['price'])[1];
      $company['links']          = $this->getSocialLinksForCompany($company['company_id']);
      $company['lat']            = $company['lat'] + 0;
      $company['lng']            = $company['lng'] + 0;
      $company['hours']          = $this->getOpeningHoursForCompany($company['company_id']);
      $company['related']        = $this->getAdditionalSuggestionsForSameCompany($company['company_id'], $company['_id']);
      $googleRelated             = $this->getAdditionalSuggestionsForSameCompanyByGooglePlaceId($company['_id'], $company['google_place_id']);

      if(count($googleRelated) > 0) {
        $company['related'] = array_merge($company['related'], $googleRelated);
        $company['related'] = array_unique($company['related'], SORT_REGULAR);
      }

      $company['links'] = $this->getSocialLinksForCompany($company['company_id']);

      $response[] = $company;
    }
    return $response;
  }

  public function getImagePathByAssortmentId($aId){
    $stmt = "SELECT img_path FROM assortment where id = :id";
    $values = [":id" => $aId];
    return parent::pQuery($stmt, $values);

  }

  public function deactivate($aId){
    $stmt = "UPDATE assortment set is_valid = 0 WHERE id = :id";
    $values = [":id" => $aId];
    parent::pExecute($stmt, $values);
  }

  public function getNewest($lat, $long, $distance = 15, $offset = 1, $order = "created_dtm", $limit = 15) {
    $stmt = "
      SELECT
            ( 6371 * acos( cos( radians({$lat}) ) * cos( radians( c.lat ) ) * cos( radians( c.lng ) - radians({$long}) ) + sin( radians({$lat}) ) * sin( radians( c.lat ) ) ) ) AS 'distance',
            round(( 6371 * acos( cos( radians({$lat}) ) * cos( radians( c.lat ) ) * cos( radians( c.lng ) - radians({$long}) ) + sin( radians({$lat}) ) * sin( radians( c.lat ) ) ) ), 2) AS 'distance_accurate',
             a.id as _id,
             c.id as company_id,
             c.google_place_id,
             4 as rating,
             a.title as title_food,
             a.description as description_food,
             REPLACE(price, '.', ',') as price,
             a.img_path,
             a.user_type as 'usrType',
             c.title as title_company,
             c.description as description_company,
             c.phone,
             c.street,
             c.street_number,
             c.zip,
             c.city,
             c.website,
             c.lat,
             c.lng,
             c.country,
             c.facebook_id,
             a.created_dtm,
             CONCAT('https://graph.facebook.com/v2.8/', c.facebook_id, '/picture') as logo_path
        FROM assortment a
        JOIN company c
          ON a.company_id = c.id
       WHERE a.is_valid = 1
         AND a.visible = 1
      HAVING distance_accurate < 15
    ORDER BY {$order} DESC, a.id DESC, usrType
       LIMIT {$limit}
      OFFSET {$offset}";

    $result = parent::query($stmt);
    $result = $result->fetchAll(PDO::FETCH_ASSOC);

    if(count($result) < 1 && $distance < 60) {
      $distance = $distance + 15;
      $this->getNearByAssortments($lat, $long, $distance, $offset, $order);
    }
    $response = [];
    foreach ($result as $company) {
      $company['price_digit']    = explode(",", $company['price'])[0];;
      $company['price_decimal']  = explode(",", $company['price'])[1] == "00" ? "" : explode(",", $company['price'])[1];
      $company['lat']            = $company['lat'] + 0;
      $company['lng']            = $company['lng'] + 0;
      $company['hours']          = $this->getOpeningHoursForCompany($company['company_id']);
      $company['related']        = $this->getAdditionalSuggestionsForSameCompany($company['company_id'], $company['_id']);
      $googleRelated             = $this->getAdditionalSuggestionsForSameCompanyByGooglePlaceId($company['_id'], $company['google_place_id']);

      if(count($googleRelated) > 0) {
        $company['related'] = array_merge($company['related'], $googleRelated);
        $related = array_values(array_unique($company['related'], SORT_REGULAR));
        $company['related'] = $related;
      }

      $company['links']          = $this->getSocialLinksForCompany($company['company_id']);

      if(floatval($company['distance']) < 1) {
        $floatDistance = floatval($company['distance']);
        $nearest50 = round(($floatDistance * 100) / 50) * 50;
        $company['distance'] = $nearest50;
        $company['distance_unit'] = "m";
      }else {
        $floatDistance = floatval($company['distance']);
        $company['distance'] = round($floatDistance, 1);
        $company['distance_unit'] = "km";
      }

      $response[] = $company;
    }

    return $response;
  }

}
