<?php

require_once "../lib/get/db/db.php";

class get_db_Users extends get_DB {

  public function save($user){
    if($this->fetch($user->fbId) == false) {
      $user = $this->persist($user);
      return $user;
    }
    return false;
  }

  public function fetch($fbId) {
    $stmt   = "SELECT * FROM user WHERE facebook_id = {$fbId}";
    $result = parent::query($stmt);
    $result = $result->fetch(PDO::FETCH_ASSOC);

    if(isset($result['id'])) {
      return $result;
    }
    return false;
  }

  public function getUserById($userId){
    $stmt   = "SELECT * FROM user WHERE id = {$userId}";
    $result = parent::query($stmt);
    $result = $result->fetch(PDO::FETCH_ASSOC);

    if(isset($result['id'])) {
      return $result;
    }
    return false;
  }

  public function saveTokenForUser($user, $token, $fbToken) {
    $stmt = "INSERT INTO user_access(user_id, fb_token, token, valid, date) VALUES (:user_id, :fb_token, :token, :valid, :date)";
    $values = [
      ":user_id"  => $user,
      ":fb_token" => $fbToken,
      ":token"    => $token,
      ":valid"    => 1,
      ":date"     => date("Y-m-d H:i:s"),
    ];
    parent::insert($stmt, $values);

  }

  public function getUserAccessForUser($userId){
    $stmt = "SELECT ua.user_id, ua.token, c.title as companyName, c.id as companyId
                  FROM user_access ua
             LEFT JOIN company c
                    ON c.id = (SELECT company_id from company_user where user_id = {$userId})
                 WHERE ua.user_id = {$userId}";
    $result = parent::query($stmt);
    $result = $result->fetch(PDO::FETCH_ASSOC);
    if(isset($result['token'])) {
      return $result;
    }
    return false;
  }

  public function getUserAccessForToken($token) {
    $stmt = "SELECT ua.user_id, token as token, title as company_name
              FROM user_access ua
         LEFT JOIN company c
                ON ua.user_id = c.user_id
             WHERE ua.token = :token";
    $values = [":token" => $token];
    $result = parent::pQuery($stmt, $values);

    if(isset($result['token'])) {
      return $result;
    }
    return false;
  }

  public function getCompanyUserForUser($userId){
    $stmt = "SELECT company_id FROM company_user WHERE user_id = {$userId}";
    $result = parent::query($stmt);
    $result = $result->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  public function getCompanyForUser($userId){
    $stmt = "SELECT company_id FROM company WHERE user_id = {$userId}";
    $result = parent::query($stmt);
    $result = $result->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  public function getCompanyUserForFacebookUser($fbId){
    $stmt = "SELECT company_id, company_user.user_id, title as company_name
               FROM company_user
               JOIN company
                 ON company_user.company_id = company.id
              WHERE company_user.user_id = (SELECT id from user where facebook_id = {$fbId})";
    $result = parent::query($stmt);
    $result = $result->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  public function getCompanyUserForCompany($cId){
    $stmt   = "SELECT * from company_user where company_id = :company_id";
    $values = [":company_id" => $cId];
    return parent::pQuery($stmt, $values);
  }

  private function persist($user){
    try {
      $stmt = "INSERT INTO user(facebook_id, firstname, lastname) VALUES (:facebook_id, :firstname, :lastname)";
      $values = [
        ":facebook_id" => $user->fbId,
        ":firstname"   => $user->firstname,
        ":lastname"    => $user->lastname,
      ];

      parent::insert($stmt, $values);
    }catch(Exception $e) {
      error_log("Insert user error");
      error_log($e);
    }
    return parent::getLastId();
  }

}
