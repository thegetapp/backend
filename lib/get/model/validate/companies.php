<?php

require_once("validator.php");

class get_model_validate_Companies implements get_model_validate_IValidator {

	public function validateRequest($data){

		if( isset($data->serviceToken) &&
			isset($data->google_place_id) && 
			isset($data->country) &&
		 	isset($data->street) && 
		 	isset($data->zip) && 
		 	isset($data->phone) &&
		 	isset($data->website) &&
		 	isset($data->title) &&
		 	isset($data->description) &&
		 	isset($data->lng) &&
		 	isset($data->lat) &&
			isset($data->links) &&
			isset($data->hours)) {
			error_log("Successfully validated companies request: ");
			return true;
			
		}else {
			error_log("Error validating companies request: " . json_encode($data));
			return false;
		}


	}

	public function validateResponse($data){

	}
}