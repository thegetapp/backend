<?php

require_once("validator.php");

class get_model_validate_Upload implements get_model_validate_IValidator {

	public function validateRequest($data){

		if( isset($data->serviceToken) &&
			isset($data->imageSourceBase64) &&
			isset($data->title_food) &&
			isset($data->price) &&
			isset($data->description_food) &&
			isset($data->usrType)) {
			error_log("Successfully validated upload request: ");
			return true;

		}else {
			error_log("Error validating upload request: " . json_encode($data));
			return false;
		}
	}

	public function validateResponse($data){

	}

	public function validateRequestFromReviewer($data){
		if( isset($data->serviceToken) &&
      (isset($data->imageSourceBase64) || isset($data->img_path) )&&
			
			isset($data->title_food) &&
			isset($data->price) &&
			isset($data->description_food) &&

			isset($data->google_place_id) &&
			isset($data->company_title) &&
			isset($data->lat) &&
			isset($data->lng) &&
			isset($data->country) &&
			isset($data->city) &&
			isset($data->zip) &&
			isset($data->street) &&
			isset($data->usrType)) {
			error_log("Successfully validates upload [REVIEWER] request: ");
			return true;
		}else {
			error_log("Error validating upload [REVIEWER] request: " . json_encode($data));
			return false;
		}
	}
	public function validateRequestFromReviewerEditor($data){
		if( isset($data->serviceToken) &&
      (isset($data->imageSourceBase64) || isset($data->img_path) )&&

			isset($data->title_food) &&
			isset($data->price) &&
			isset($data->description_food) &&

			isset($data->company_title) &&
			isset($data->lat) &&
			isset($data->lng) &&
			isset($data->country) &&
			isset($data->city) &&
			isset($data->zip) &&
			isset($data->street) &&
			isset($data->usrType)) {
			error_log("Successfully validates upload [REVIEWER_EDITOR] request: ");
			return true;
		}else {
			error_log("Error validating upload [REVIEWER_EDITOR] request: " . json_encode($data));
			return false;
		}
	}
  public function validateRequestFromCompanyEditor($data){
    if( isset($data->serviceToken) &&
      (isset($data->imageSourceBase64) || isset($data->img_path) )&&

      isset($data->title_food) &&
      isset($data->price) &&
      isset($data->description_food) &&

      isset($data->usrType)) {
      error_log("Successfully validates upload [COMPANY_EDITOR] request: ");
      return true;
    }else {
      error_log("Error validating upload [COMPANY_EDITOR] request: " . json_encode($data));
      return false;
    }
  }
}