<?php

require_once("validator.php");

class get_model_validate_Assortment implements get_model_validate_IValidator {


	public function validateRequest($data){
		if(isset($data[0]->lat) && isset($data[0]->long)) {

			error_log("Successfully validated assortment request: " . json_encode($data));
			return true;
		}else {
			
			error_log("Error validating assortment request: " . json_encode($data));
			return false;
		}

	}

	public function validateResponse($data){

	}

	public function validateUpdateRequest($data) {
		if(isset($data[0]->assortment_id)) {

			error_log("Successfully validated assortment request: ");
			return true;
		}else {
			
			error_log("Error validating assortment request: " . json_encode($data));
			return false;
		}
	}

	public function validateUpdateRequestFromReviewer($data) {
		if(isset($data[0]->lat) && isset($data[0]->long) && isset($data[0]->assortment_id)) {

			error_log("Successfully validated assortment request: ");
			return true;
		}else {
			
			error_log("Error validating assortment request: " . json_encode($data));
			return false;
		}
	}
}