<?php

interface get_model_validate_IValidator {

  public function validateRequest($data);

  public function validateResponse($data);

}
