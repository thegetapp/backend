<?php

require_once("validator.php");


class get_model_validate_Users implements get_model_validate_IValidator {

	public function validateRequest($data){

		if( isset($data->fbToken) &&
			isset($data->fbId) &&
			isset($data->firstname) &&
			isset($data->lastname)) {
			error_log("Successfully validated user request: ");
			return true;
		}else {
			error_log("Error validating user request: " . json_encode($data));
			return false;
		}


	}

	public function validateResponse($data){

	}
}