<?php

class rest_Response extends rest_Object implements rest_IResponse {
  public function getStatus() {
    return $this->safeGet('status');
  }
  public function getMessage() {
    return $this->safeGet('message');
  }
  public function getHeaders() {
    return $this->safeGet('headers');
  }
  public function getBody() {
    return $this->safeGet('body');
  }
}