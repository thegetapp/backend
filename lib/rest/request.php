<?php

class rest_Request extends rest_Object implements rest_IRequest {

  public function getUri() {
    return $this->safeGet('uri');
  }
  public function getUrl() {
    return $this->safeGet('url');
  }
  public function getServerProtocol() {
    return $this->safeGet('server_protocol');
  }
  public function getProtocol() {
    return $this->safeGet('protocol');
  }
  public function getHost() {
    return $this->safeGet('host');
  }
  public function getClientIp() {
    return $this->safeGet('client_ip');
  }
  public function getAccept() {
    return $this->safeGet('accept');
  }
  public function getAcceptExt() {
    return $this->safeGet('accept_ext');
  }
  public function getContentType() {
    return $this->safeGet('content_type');
  }
  public function getMethod() {
    return $this->safeGet('method');
  }
  public function getHeaders() {
    return $this->safeGet('headers');
  }
  public function getBody() {
    return $this->safeGet('body');
  }
  public function getRoute() {
    return $this->safeGet('route');
  }
  public function getVersion() {
    return $this->safeGet('version');
  }
  public function getPathParam() {
    return $this->safeGet('pathParam');
  }

}
