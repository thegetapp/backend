<?php

class rest_Router implements rest_IRouter {

  private $routes;

  public function __construct(array $routes = []) { // array of rest_IRoute
    $this->routes = $routes;
  }

  // @return rest_IRoute
  public function match($route) {
    foreach ($this->routes as $candidate) {
      if (preg_match($candidate->getRegex(), $route)) {
        return $candidate;
      }
    }
    return null;
  }

  // @return array of string
  public function getRoutes() {
    $r = [];
    foreach ($this->routes as $route) {
      $r[] = $route->getRoute();
    }
    return $r;
  }

}