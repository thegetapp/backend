<?php

class rest_Route extends rest_Object implements rest_IRoute {
  public function getRegex() {
    return $this->safeGet('regex');
  }
  public function getClassName() {
    return $this->safeGet('classname');
  }
  public function getRoute() {
    return $this->safeGet('route');
  }
}