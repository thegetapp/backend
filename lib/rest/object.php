<?php

class rest_Object {

  private $row;

  public function __construct(array $row) {
    $this->row = $row;
  }

  protected function safeGet($key) {
    return ( isset($this->row[$key]) ? $this->row[$key] : null);
  }
 
}