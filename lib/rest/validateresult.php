<?php

class rest_ValidateResult extends rest_Object implements rest_IValidateResult {

  // @return boolean
  public function isValid() {
    return $this->safeGet('valid');
  }

  // @return string
  public function getMessage() {
    return $this->safeGet('message');
  }

}