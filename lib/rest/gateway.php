<?php

class rest_Gateway implements rest_IGateway {

  private $router;

  public function __construct(rest_IRouter $router) {
    $this->router = $router;
  }

  // @return boolean
  public function validateRequest($uniqueId, rest_IRequest $request) {

    // Only support json
    if ( $request->getAcceptExt() != 'json' ) {
      return false;
    }

    if ( is_null($request->getBody()) ) {
      return true;
    }
    return $this->validateJson($request->getBody());
  }

  // @return boolean
  public function validateResponse($uniqueId, rest_IResponse $response) {
    if ( is_null($response->getBody()) ) {
      return true;
    }
    return $this->validateJson($response->getBody());
  }


  // @return rest_IRoute
  public function fetchRouteForRequest($uniqueId, rest_IRequest $request) {
    return $this->router->match($request->getRoute());
  }

  // @return rest_IValidateResult
  public function validateNoMaintenance($uniqueId, rest_IRequest $request) {
    return new rest_ValidateResult( ['valid' => true] );
  }

  // @return rest_IValidateResult
  public function authenticateRequest($uniqueId, rest_IRequest $request) {
    return new rest_ValidateResult( ['valid' => true ] );
  }

  // @NOTE: The rest framework only authorize that the key is allow to execute the resolved route.
  //        It is the job of the resource controller to assert if the key is allow to access the data that the controller will process.
  // @return rest_IValidateResult
  public function authorizeRequest($uniqueId, rest_IRequest $request) {
    return new rest_ValidateResult( ['valid' => true] );
  }

  // rest_IResource
  public function instantiateResourceForRoute($uniqueId, rest_IRoute $route) {
    $class = $route->getClassName();
    if (empty($class)) {
      return null;
    }

    if ( !class_exists($class, true) ) {
      return null;
    }
    return new $class();
  }

  // @return void
  public function logExceptionForRequest($uniqueId, rest_IRequest $request, Exception $e) {
    $message  = "[".$request->getMethod()."] [".$request->getRoute()."] ";
    $message .= get_class($e) .": ".$e->getMessage()." in ".$e->getFile()." on line ".$e->getLine() . " trace is " . $e->getTraceAsString();
    $this->errorLog($uniqueId, $message);
  }


  // @return void
  public function typeCheck($obj, array $interfaces) {
    if ( !is_object($obj) ) {
      throw new rest_Exception("Not an object.");
    }

    if ( !self::implementsInterfaces($obj, $interfaces) ) {
      throw new rest_Exception("Object '".get_class($obj)."' must implement required interfaces: " . implode(",", $interfaces));
    }
  }

  // @return boolean
  public function implementsInterfaces($obj, array $interfaces) {
    if ( !is_object($obj) )
      return false;

    $implementedInterfaces = class_implements($obj, false);
    $implementsRequired = array_intersect($interfaces, $implementedInterfaces);
    if ( empty($implementsRequired) ) {
      return false;
    }
    return true;
  }


  // @return boolean
  private function validateJson($str) {
    if ( !is_string($str) ) {
      return false;
    }

    json_decode($str);
    if ( json_last_error() === JSON_ERROR_NONE ) {
      return true;
    }

    return false; // deny by default.
  }

  // @return void
  private function errorLog($uniqueId, $message) {
    error_log("[".$uniqueId."] " . $message);
  }

}