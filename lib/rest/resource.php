<?php

class rest_Resource implements rest_IResource {

  // Error handling, we use 3 http reponse codes for resource controllers. The framework it self will use a few more.
  // Good reading: https://blog.apigee.com/detail/restful_api_design_what_about_errors

  // Everything OK (200)
  public function jsonOk(array $data) {
    return $this->jsonResult(200, $data);
  }

  // Client application made an error (404)
  public function jsonErrorApplication(array $data) {
    return $this->jsonResult(404, $data);
  }

  // Api made an error (500)
  public function jsonErrorApi(array $data) {
    return $this->jsonResult(500, $data);
  }

  // @return rest_IResponse | null
  final public function execute($uniqueId, rest_IRequest $request) {
    $method = strtolower($request->getMethod()) . ucfirst($request->getAcceptExt());
    $response = $this->callMethod($uniqueId, $request, $method);
    return $response;
  }

  // @return rest_IResponse
  private function callMethod($uniqueId, rest_IRequest $request, $method) {
    if ( method_exists($this, $method) ) {
      return $this->{$method}($uniqueId, $request);
    }
    return null;
  }

  // @return rest_Response
  private function jsonResult($status, array $data) {
    $json = json_encode($data);
    if ( json_last_error() !== JSON_ERROR_NONE ) {
      throw new Exception("Unable to encode json. " . json_last_error_msg());
    }

    $row = [
      'status' => intval($status),
      'headers' => [
        'Content-Type: application/json; charset=utf-8',
        'Content-Length: ' . strlen($json)
      ],
      'body' => $json
    ];
    return new rest_Response($row);
  }

}
