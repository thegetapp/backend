<?php

spl_autoload_register('rest_autoload');
set_error_handler('rest_error_handler');

interface IRest {
  // @return int
  public function run(rest_IGateway $gateway);
}

interface rest_IGateway {

  // @return boolean
  public function validateRequest($uniqueId, rest_IRequest $request);

  // @return boolean
  public function validateResponse($uniqueId, rest_IResponse $response);

  // rest_IRoute
  public function fetchRouteForRequest($uniqueId, rest_IRequest $request);

  // rest_IValidateResult
  public function validateNoMaintenance($uniqueId, rest_IRequest $request);
  public function authenticateRequest($uniqueId, rest_IRequest $request);
  public function authorizeRequest($uniqueId, rest_IRequest $request);

  // rest_IResource
  public function instantiateResourceForRoute($uniqueId, rest_IRoute $route);

  // @return void
  public function logExceptionForRequest($uniqueId, rest_IRequest $request, Exception $e);

  // @return void
  public function typeCheck($obj, array $interfaces);

  // @return boolean
  public function implementsInterfaces($obj, array $interfaces);

}

interface rest_IRouter {
  // @return rest_IRoute
  public function match($route);
}

interface rest_IRoute {
  public function getRegex();
  public function getClassName();
  public function getRoute();
}

interface rest_IValidateResult {
  public function isValid();
  public function getMessage();
}

interface rest_IRequest {
  // @return string
  public function getUri();
  public function getUrl();
  public function getServerProtocol();
  public function getProtocol();
  public function getHost();
  public function getClientIp();
  public function getAccept();
  public function getAcceptExt();
  public function getContentType();
  public function getMethod();
  public function getHeaders();
  public function getBody();
  public function getRoute();
  public function getPathParam();
}

interface rest_IResponse {
  // @return string
  public function getStatus();
  public function getMessage();

  // @return array
  public function getHeaders();

  // @return string
  public function getBody();
}

interface rest_IResource {
  // @return rest_IResponse | null
  public function execute($uniqueId, rest_IRequest $request);
}

// @INFO: We want the API to always log enough data for basic performance analysis. This is done by the webserver trough the access log.
// Format: [Request Received Time] [Process Id] [Thread Id] [Unique Id] [Remote IP] [Request in bytes] [Accept] [Route] [Method] [HTTP Status Code] [Content-Type] [Bytes returned to client no headers] [Response in bytes] [Response time in microseconds]
//
// Apache example:
// LogFormat "%{%FT%TZ}t %{pid}P %{tid}P %{UNIQUE_ID}n %a %I \"%{Accept}i\" %U %m %>s \"%{Content-Type}o\" %B %O %D %{PHP_PROCESS_TIME}n \"%{Authorization}i\"" performance
// CustomLog /home/mnk/logs/mnk-rest-performance_log performance
class Rest implements IRest {

  public function run(rest_IGateway $gateway) {
    $startTime = microtime(true);
    $uniqueId = $this->fetchUniqueId();

    $this->logStartProcessing($uniqueId);

    $request = $this->parseRequest();

    ob_start(null, 0, PHP_OUTPUT_HANDLER_STDFLAGS ^ PHP_OUTPUT_HANDLER_REMOVABLE ^ PHP_OUTPUT_HANDLER_FLUSHABLE);

    try {

      // Sanity check. Request must be valid
      if ( $gateway->validateRequest($uniqueId, $request) !== true ) {
        $response = new rest_Response( ['status' => self::UNSUPPORTED_MEDIA_TYPE] );
        return $this->send($uniqueId, $startTime, $request, $response);
      }

      // Resolve route
      $route = $gateway->fetchRouteForRequest($uniqueId, $request);
      if ( empty($route) ) {
        $response = new rest_Response( ['status' => self::CLIENT_ERROR] );
        return $this->send($uniqueId, $startTime, $request, $response);
      }
      $gateway->typeCheck($route, ['rest_IRoute']);

      // Check for maintenance
      $validateResult = $gateway->validateNoMaintenance($uniqueId, $request);
      $gateway->typeCheck($validateResult, ['rest_IValidateResult']);
      if ( !$validateResult->isValid() ) {
        $response = new rest_Response( ['status' => self::MAINTENANCE, 'body' => $validateResult->getMessage()] );
        return $this->send($uniqueId, $startTime, $request, $response);
      }

      // Authenticate
      $validateResult = $gateway->authenticateRequest($uniqueId, $request);
      $gateway->typeCheck($validateResult, ['rest_IValidateResult']);
      if ( !$validateResult->isValid() ) {
        $response = new rest_Response( ['status' => self::UNAUTHORIZED, 'body' => $validateResult->getMessage()] );
        return $this->send($uniqueId, $startTime, $request, $response);
      }

      // Authorize
      $validateResult = $gateway->authorizeRequest($uniqueId, $request);
      $gateway->typeCheck($validateResult, ['rest_IValidateResult']);
      if ( !$validateResult->isValid() ) {
        $response = new rest_Response( ['status' => self::FORBIDDEN, 'body' => $validateResult->getMessage()] );
        return $this->send($uniqueId, $startTime, $request, $response);
      }

      // Instantiate resource
      $resource = $gateway->instantiateResourceForRoute($uniqueId, $route);
      if ( empty($resource) ) {
        $response = new rest_Response( ['status' => self::CLIENT_ERROR] );
        return $this->send($uniqueId, $startTime, $request, $response);
      }
      $gateway->typeCheck($resource, ['rest_IResource']);


      // Delegate control to the resource
      $response = $resource->execute($uniqueId, $request);
      if ( empty($response) ) {
        $response = new rest_Response( ['status' => self::CLIENT_ERROR] );
        return $this->send($uniqueId, $startTime, $request, $response);
      }
      $gateway->typeCheck($response, ['rest_IResponse']);

      // Sanity check. Response must be valid
      if ( $gateway->validateResponse($uniqueId, $response) !== true ) {
        throw new Exception("Response returned from resource is not valid."); // 500
      }

      // Send
      return $this->send($uniqueId, $startTime, $request, $response);
    } catch (Exception $e) {
      $gateway->logExceptionForRequest($uniqueId, $request, $e);
      $response = new rest_Response( ['status' => self::SERVER_ERROR] );
      return $this->send($uniqueId, $startTime, $request, $response);
    }
  }

  // @return int
  private function send($uniqueId, $startTime, rest_IRequest $request, rest_IResponse $response) {
    ob_clean(); // Prevent anything but the response to be sent to the client.

    $status = intval($response->getStatus());
    $message = $response->getMessage();
    if ( empty($message) ) {
      $message = $this->lookupDefaultMessageForStatus($status);
    }

    $statusHeader = $_SERVER['SERVER_PROTOCOL']. " " . $status . " " . $message;
    $headers = $response->getHeaders();
    if ( empty($headers) ) {
      $headers = [
        'Content-Type: ' . $request->getAccept()
      ];
    }

    $body = $response->getBody();
    $len = strlen($body);
    if ( $len > 0 ) {
      $headers[] = "Content-Length: " . $len;
    }

    ob_start();
    foreach ($headers as $header) {
      header($header);
    }
    header($statusHeader, true, $status);

    echo $body;
    ob_end_flush();

    $processTimeInMs = ceil((microtime(true) - $startTime) * 1000);
    $this->logStopProcessing($uniqueId, $processTimeInMs);
    return $status;
  }

  // @return string
  private function lookupDefaultMessageForStatus($status) {
    // HTTP/1.1 - http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
    switch ( intval($status) ) {
    case 100: return "Continue";
    case 101: return "Switching Protocols";

    case 200: return "OK";
    case 201: return "Created";
    case 202: return "Accepted";
    case 203: return "Non-Authoritative Information";
    case 204: return "No Content";
    case 205: return "Reset Content";
    case 206: return "Partial Content";

    case 300: return "Multiple Choices";
    case 301: return "Moved Permanently";
    case 302: return "Found";
    case 303: return "See Other";
    case 304: return "Not Modified";
    case 305: return "Use Proxy";
    case 307: return "Temporary Redirect";

    case 400: return "Bad Request";
    case 401: return "Unauthorized";
    case 402: return "Payment Required";
    case 403: return "Forbidden";
    case 404: return "Not Found";
    case 405: return "Method Not Allowed";
    case 406: return "Not Acceptable";
    case 407: return "Proxy Authentication Required";
    case 408: return "Request Timeout";
    case 409: return "Conflict";
    case 410: return "Gone";
    case 411: return "Length Required";
    case 412: return "Precondition Failed";
    case 413: return "Request Entity Too Large";
    case 414: return "Request-URI Too Long";
    case 415: return "Unsupported Media Type";
    case 416: return "Requested Range Not Satisfiable";
    case 417: return "Expectation Failed";

    case 500: return "Internal Server Error";
    case 501: return "Not Implemented";
    case 502: return "Bad Gateway";
    case 503: return "Service Unavailable";
    case 504: return "Gateway Timeout";
    case 505: return "HTTP Version Not Supported";
    }
    return "Unknown";
  }

  // @return rest_IRequest
  private function parseRequest() {
    $row = [];
    if ( isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI']) ) {
      $row['uri'] = $_SERVER['REQUEST_URI'];
    }

    $row['protocol'] = 'http';
    if ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
      $row['protocol'] = 'https';
    }

    if ( isset($_SERVER['SERVER_PROTOCOL']) && !empty($_SERVER['SERVER_PROTOCOL']) ) {
      $row['server_protocol'] = $_SERVER['SERVER_PROTOCOL'];
    }

    if ( isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST']) ) {
      $row['host'] = $_SERVER['HTTP_HOST'];
    }

    if ( isset($_SERVER['REMOTE_ADDR']) && !empty($_SERVER['REMOTE_ADDR']) ) {
      $row['client_ip'] = $_SERVER['REMOTE_ADDR'];
    }

    if ( isset($_SERVER['REQUEST_METHOD']) && !empty($_SERVER['REQUEST_METHOD']) ) {
      $row['method'] = $_SERVER['REQUEST_METHOD'];
    }
    if ( isset($_SERVER['HTTP_METHOD']) && !empty($_SERVER['HTTP_METHOD']) ) {
      $row['method'] = $_SERVER['HTTP_METHOD'];
    }
    $row['method'] = strtolower($row['method']);

    $body = file_get_contents('php://input');
    if ( isset($body) && !empty($body) ) {
      $row['body'] = $body;
    }

    $headers = [];
    foreach($_SERVER as $i => $val) {
      if ( strpos($i, 'HTTP_') === 0 || in_array($i, ['CONTENT_TYPE', 'CONTENT_LENGTH']) ) {
        $name = str_replace(['HTTP_', '_'], ['', '-'], $i);
        $headers[] = "$name: $val";
      }
    }
    $row['headers'] = $headers;

    $row['url'] = $row['protocol'] . "://" . $row['host'] . $row['uri'];
    $row['route'] = strtolower($row['uri']);
    $row['version'] = strtok($row['route'], '/');

    preg_match("~/\d.*$~", $row['route'], $id);
    if($id && $id[0]) {
      $id = substr($id[0], 1);
      if(is_numeric(intval($id))) {
        $row['pathParam'] = $id;
      }
    }

    if ( isset($_SERVER['HTTP_ACCEPT']) && !empty($_SERVER['HTTP_ACCEPT']) ) {
      if ( strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false ) {
        $row['accept'] = "application/json";
        $row['accept_ext'] = "json";
      }
    }

    if ( isset($_SERVER['CONTENT_TYPE']) && !empty($_SERVER['CONTENT_TYPE']) ) {
      $row['content_type'] = $_SERVER['CONTENT_TYPE'];
    }

    return new rest_Request($row);
  }

  private function logStartProcessing($uniqueId) {
    if (function_exists("apache_note")) {
      apache_note('UNIQUE_ID', $uniqueId);
    }
  }

  private function logStopProcessing($uniqueId, $phpProcessingTimeInMs) {
    if (function_exists("apache_note")) {
      apache_note('PHP_PROCESS_TIME', $phpProcessingTimeInMs);
    }
  }

  // @return string
  private function fetchUniqueId() {
      // UUID v.4, found in the php comments http://www.php.net/manual/en/function.uniqid.php#94959
    return sprintf(
      '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      // 32 bits for "time_low"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),
      // 16 bits for "time_mid"
      mt_rand(0, 0xffff),
      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      mt_rand(0, 0x0fff) | 0x4000,
      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      mt_rand(0, 0x3fff) | 0x8000,
      // 48 bits for "node"
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
  }

  // HTTP status codes used by the framework.
  const CLIENT_ERROR = 404;
  const SERVER_ERROR = 500;
  const OK = 200;
  const MAINTENANCE = 503;
  const UNSUPPORTED_MEDIA_TYPE = 415;
  const BAD_REQUEST = 400;
  const FORBIDDEN = 403;
  const UNAUTHORIZED = 401;

}

function rest_search_include_path($fileName) {
  if (is_file($fileName)) {
    return $fileName;
  }
  foreach (explode(PATH_SEPARATOR, ini_get("include_path")) as $path) {
    if (strlen($path) > 0 && $path{strlen($path) - 1} != DIRECTORY_SEPARATOR) {
      $path .= DIRECTORY_SEPARATOR;
    }
    $f = realpath($path . $fileName);
    if ($f && is_file($f)) {
      return $f;
    }
  }
  return false;
}

function rest_autoload($className) {
  // Using _ as directory seperator.
  $fileName = str_replace('_', DIRECTORY_SEPARATOR, strtolower($className)).'.php';
  if (rest_search_include_path($fileName)) {
    require_once($fileName);
    return;
  }
}

function rest_error_handler($severity, $message, $filename, $lineno) {
  if (error_reporting() == 0) {
    return;
  }
  if (error_reporting() & $severity) {
    throw new ErrorException($message, 0, $severity, $filename, $lineno);
  }
}
