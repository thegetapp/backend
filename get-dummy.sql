INSERT INTO assortment(id, title, company_id, user_id, price, description, img_path) VALUES(
  1,
  "Lobster, onion and lavender",
  0,
  0,
  65,
  "Svinemørbrad pudses ren for sener og skæres i skiver på 3 cm. og bankes let.
Herefter steges skiverne i en gryde med margarine og karry (ca. 1 spsk stærk karry. eller efter smag) - salt og peber efter behov på mørbradbøfferne,",
  "https://charlotteagenda-charlotteagenda.netdna-ssl.com/wp-content/uploads/2015/11/bbq-plate.jpg"
);

INSERT INTO company(user_id, street, street_number, country, zip, phone, website, title, lng, lat, description, google_) VALUES (
  0,
  "Strandgade",
  "12",
  "Danmark",
  1155,
  32963297,
  "http://hygge.dk",
  "Hygge Kroen",
  55.677903,
  12.5941113,
  "Bromølle Kro er omgivet af skov og idyllisk beliggende ved Åmose Å, hvor der er rig mulighed for vandreture og for at følge årstidernes skiften på tætteste hold. Bromølle Kro har ligget på dette smukke sted siden 1198- midt i Vestsjællands naturpark."
);


INSERT INTO assortment(id, title, company_id, user_id, price, description, img_path) VALUES(
  3,
  "Flæske tæske godt tilbud!",
  1,
  0,
  125,
  "Stegt flæsk med persillesovs er en kendt dansk middagsret bestående af ovn- eller pandestegte skiver flæsk, der serveres med kogte kartofler og persillesovs. Der kan anvendes kød fra stegestykket eller kogestykket.",
  "http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/5151/4_880.jpg"
);

INSERT INTO assortment(id, title, company_id, user_id, price, description, img_path) VALUES(
  3,
  "Flæske tæske godt tilbud!",
  1,
  0,
  125,
  "Stegt flæsk med persillesovs er en kendt dansk middagsret bestående af ovn- eller pandestegte skiver flæsk, der serveres med kogte kartofler og persillesovs. Der kan anvendes kød fra stegestykket eller kogestykket.",
  "http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/5151/4_880.jpg"
);

INSERT INTO company(user_id, street, street_number, city, country, zip, phone, website, title, lng, lat, description) VALUES (
  0,
  "Sluseholmen",
  "34",
  "København SV",
  "Danmark",
  2450,
  32963297,
  "http://SukuriSushi.dk",
  "Sukuri Sushi",
  55.677903,
  12.5941113,
  "Bromølle Kro er omgivet af skov og idyllisk beliggende ved Åmose Å, hvor der er rig mulighed for vandreture og for at følge årstidernes skiften på tætteste hold. Bromølle Kro har ligget på dette smukke sted siden 1198- midt i Vestsjællands naturpark."
);

INSERT INTO assortment(id, title, company_id, user_id, price, description, img_path) VALUES(
  4,
  "Laks od",
  2,
  0,
  85,
  "Nigiri laks, laks new york, soyagravad laks og flammegrillet laks. Hosomaki inside-out med laks, agurk, miso aïoli & hvidløg daikon med laks",
  "https://roomservice2.s3.amazonaws.com/Product/55492/U8HyFfEx1UOMiyugE8QaRA/SquareHighRes.png"
);

INSERT INTO assortment(id, title, company_id, user_id, price, description, img_path) VALUES(
  5,
  "Mini maki maki",
  2,
  0,
  240,
  "Kaburimaki: hell's kitchen, ebi panko, shake aioli & lakse ceviche. Soya, wasabi og ingefær. (Bemærk: billede fra maki maki)",
  "https://roomservice2.s3.amazonaws.com/Product/55492/U8HyFfEx1UOMiyugE8QaRA/SquareHighRes.png"
);

INSERT INTO company(user_id, street, street_number, city, country, zip, phone, website, title, lat, lng, description) VALUES (
  0,
  "Fjordgade",
  "12",
  "Aalborg",
  "Danmark",
  9000,
  32963297,
  "http://aalborg.dk",
  "Aalborg Sushi",
  57.048820,
  9.921747,
  "Bromølle Kro er omgivet af skov og idyllisk beliggende ved Åmose Å, hvor der er rig mulighed for vandreture og for at følge årstidernes skiften på tætteste hold. Bromølle Kro har ligget på dette smukke sted siden 1198- midt i Vestsjællands naturpark."
);

INSERT INTO assortment(id, title, company_id, user_id, price, description, img_path) VALUES(
  6,
  "Mini maki maki",
  2,
  0,
  240,
  "Jeg havde valgt at fejre min fødselsdag på Restaurant Green Solution House - bedste beslutning i lang tid!",
  "https://media-cdn.tripadvisor.com/media/photo-s/07/fa/b6/92/lokal-oko-gris.jpg"
);
