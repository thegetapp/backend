<?php

class resource_v1_Webadmin extends resource_Base {

  public function postJson($uniqueId, rest_IRequest $request) {
    $body = json_decode($request->getBody());

    $validated = $this->registry->ValUsers->validateRequest($body);
    if(!$validated){
      return $this->jsonErrorApplication(["error" => "Invalid request"]);
    }

    $response = $this->registry->Auth->generateToken($body);

    return $this->jsonOk([$response]);

  }

  public function getJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(["Error" => "Not implemented"]);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}
