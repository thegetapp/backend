<?php

require_once "../lib/get/db/assortments.php";
require_once "../lib/get/db/users.php";
require_once "../lib/get/db/db.php";
require_once "../lib/get/db/companies.php";

require_once "../lib/get/service/authorization.php";

require_once "../lib/amazon/get/cloud.php";

require_once "../lib/get/model/validate/users.php";
require_once "../lib/get/model/validate/companies.php";
require_once "../lib/get/model/validate/upload.php";
require_once "../lib/get/model/validate/assortment.php";

class resource_Base extends rest_Resource {

  protected $registry, 
            $assortments, 
            $companies, 
            $users, 
            $auth, 
            $cloud, 
            $db, 
            $valUsers, 
            $valCompanies, 
            $valUpload, 
            $valAssortment;

  final public function __construct() {
    $this->registry      = new Registry();
    $this->assortments   = new get_db_Assortments();
    $this->companies     = new get_db_Companies();
    $this->users         = new get_db_Users();
    $this->auth          = new get_service_Authorization();
    $this->cloud         = new amazon_get_Cloud();
    $this->valUsers      = new get_model_validate_Users();
    $this->valCompanies  = new get_model_validate_Companies();
    $this->valUpload     = new get_model_validate_Upload();
    $this->valAssortment = new get_model_validate_Assortment();

    $this->registry->set($this->registry, 'Registry');
    $this->registry->set($this->assortments, 'Assortments');
    $this->registry->set($this->companies, "Companies");
    $this->registry->set($this->users, "Users");

    $this->registry->set($this->auth, "Auth");

    $this->registry->set($this->cloud, "Cloud");

    $this->registry->set($this->valUsers, "ValUsers");
    $this->registry->set($this->valCompanies, "ValCompanies");
    $this->registry->set($this->valUpload, "ValUpload");
    $this->registry->set($this->valAssortment, "ValAssortment");
  }

}
