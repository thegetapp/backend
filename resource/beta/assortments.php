<?php

class resource_beta_Assortments extends resource_Base {

  public function postJson($uniqueId, rest_IRequest $request) {
    $data = $this->registry->Assortments->fetch(json_decode($request->getBody()));
    $data = is_array($data) ? $data : [$data];
    return $this->jsonOk($data);
  }

  public function getJson($uniqueId, rest_IRequest $request) {
    $data = $this->registry->Assortments->fetch([]);
    return $this->jsonOk($data);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}
