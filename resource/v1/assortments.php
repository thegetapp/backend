<?php

class resource_v1_Assortments extends resource_Base {

  public function postJson($uniqueId, rest_IRequest $request) {
    $body = json_decode($request->getBody());

    $validated = $this->registry->ValAssortment->validateRequest($body);
    if(!$validated){
      return false;
    }

    $data = $this->registry->Assortments->fetch($body);
    $data = is_array($data) ? $data : [$data];
   
    return $this->jsonOk($data);
  }

  public function getJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(["Error" => "Not implemented"]);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}
