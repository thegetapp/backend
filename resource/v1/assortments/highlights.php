<?php

class resource_v1_assortments_Highlights extends resource_Base {

  public function postJson($uniqueId, rest_IRequest $request) {
    $body = json_decode($request->getBody());

    $validated = $this->registry->ValAssortment->validateRequest($body);
    if(!$validated){
      return false;
    }

    $data = $this->registry->Assortments->fetchHighlights($body);
    $data = is_array($data) ? $data : [$data];
   
    return $this->jsonOk($data);
  }

  public function getJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(["Error" => "Not implemented"]);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}
