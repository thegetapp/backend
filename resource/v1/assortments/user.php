<?php

class resource_v1_assortments_User extends resource_Base {


  /*
   * Gets Assortments uploaded by user
   */
  public function postJson($uniqueId, rest_IRequest $request) {
    $body = json_decode($request->getBody());
    
    $authenticated = $this->registry->Auth->authenticateUser($body);

    if(!$authenticated) {
      error_log("Could not authenticate user !");
      return  $this->jsonErrorApplication(["error" => "Authentication failed"]);
    }
    
    $data = $this->registry->Assortments->fetchAllByUserId($authenticated['user_id']);

    return $this->jsonOk(is_array($data) ? $data : [$data]);
  }

  public function getJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(["Error" => "Not implemented"]);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}
