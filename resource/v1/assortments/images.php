<?php

class resource_v1_Upload extends resource_Base {

  public function postJson($uniqueId, rest_IRequest $request) {
    $body  = $this->registry->Assortments->fetch(json_decode($request->getBody()));

    $authenticated = $this->registry->Auth->authenticateUser($body);

    if($authenticated) {

      $companyId = $this->registry->Users->getCompanyUserForUser($authenticated['user_id']);
      $data      = $this->registry->Assortment->save($body);

    }else {
      error_log("Could not authenticate user !: " . json_encode($body));
      $data = ["Error" => "Could not create authenticate user"];
    }

    $data = is_array($data) ? $data : [$data];
    return $this->jsonOk($data);
  }

  public function getJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(["Error" => "Not implemented"]);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}
