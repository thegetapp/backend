<?php

class resource_v1_assortments_Upload extends resource_Base {

  /*
   * Uploads new assortment
   */
  public function postJson($uniqueId, rest_IRequest $request) {
    $body = json_decode($request->getBody());
    error_log(json_encode($body));

    $authenticated = $this->registry->Auth->authenticateUser($body);

    if(!$authenticated) {
      error_log("Could not authenticate user !: " . json_encode($body));
      $data = ["error" => "Ups, der er opstået en fejl. Forsøg venligst at logge ud og ind igen :)"];
    }

    // Company upload
    if(isset($body->usrType) && $body->usrType == "company") {
      error_log("Company upload !" . json_encode($body));

      // Validate request
      $validated = $this->registry->ValUpload->validateRequest($body);
      if(!$validated){
        return $this->jsonErrorApplication(['error' => "request invalid." ]);
      }

      // Find company
      $company = $this->registry->Users->getCompanyUserForUser($authenticated['user_id']);
      // Upload
      $imgPath = $this->registry->Cloud->uploadFile($body->imageSourceBase64);
      $this->registry->Assortments->save($body, $authenticated['user_id'], $company['company_id'], $imgPath);
      $data = ["ok" => "Uploaded successfully"];

      $data = is_array($data) ? $data : [$data];
      return $this->jsonOk($data);

    // reviewer upload 
    }elseif (isset($body->usrType) && $body->usrType == "reviewer") {
      error_log("Reviewer upload !" . json_encode($body));

      // Validate request
      $validated = $this->registry->ValUpload->validateRequestFromReviewer($body);
      if(!$validated) {
        return $this->jsonErrorApplication(['error' => "request invalid." ]);
      }

      // Check if company already exists
      $companyId = $this->registry->Companies->getCompanyByGooglePlaceId($body->google_place_id);

      if(!$companyId['id']) {
        $companyId = $this->registry->Companies->saveReviewerCompany($body, $authenticated['user_id']);
      }else {
        $companyId = $companyId['id'];
      }

      // Upload
      $imgPath   = $this->registry->Cloud->uploadFile($body->imageSourceBase64);
      $this->registry->Assortments->save($body, $authenticated['user_id'], $companyId, $imgPath);
      $data = ["ok" => "Uploaded successfully"];

      $data = is_array($data) ? $data : [$data];
      return $this->jsonOk($data);

    }elseif(isset($body->usrType) && $body->usrType == "companyEditor") {
      error_log("Company assortment UPDATE !" . json_encode($body));

      // Validate request
      $validated = $this->registry->ValUpload->validateRequestFromCompanyEditor($body);
      if(!$validated){
        return $this->jsonErrorApplication(['error' => "request invalid." ]);
      }

      $body->usrType = "company";

      // Deactivate current assortment
      $this->registry->Assortments->deactivate($body->assortmentId);

      // Find company
      $company = $this->registry->Users->getCompanyUserForUser($authenticated['user_id']);

      // Upload
      if(isset($body->reuseImage) && $body->reuseImage == true) {
        $imgPath = $body->img_path;
      }else {
        $imgPath = $this->registry->Cloud->uploadFile($body->imageSourceBase64);
      }
      $body->usrType = "company";
      $this->registry->Assortments->save($body, $authenticated['user_id'], $company['company_id'], $imgPath);
      $data = ["ok" => "Uploaded successfully"];

      $data = is_array($data) ? $data : [$data];
      return $this->jsonOk($data);


      $data = is_array($data) ? $data : [$data];
      return $this->jsonOk($data);

    }elseif(isset($body->usrType) && $body->usrType == "reviewerEditor") {

       // Validate request
      $validated = $this->registry->ValUpload->validateRequestFromReviewerEditor($body);
      if(!$validated) {
        return $this->jsonErrorApplication(['error' => "request invalid." ]);
      }

      $body->usrType = "reviewer";

      // Deactivate current assortment
      $assortment = $this->registry->Assortments->deactivate($body->assortmentId);

      // Check if company already exists
      $companyId = $this->registry->Companies->getCompanyByGooglePlaceId($body->google_place_id);

      if(!$companyId['id']) {
        $companyId = $this->registry->Companies->saveReviewerCompany($body, $validated['user_id']);
      }else {
        $companyId = $companyId['id'];
      }

      // Upload
      if(isset($body->reuseImage) && $body->reuseImage == true) {
        $imgPath = $body->img_path;
      }else {
        $imgPath = $this->registry->Cloud->uploadFile($body->imageSourceBase64);
      }
      $body->usrType = "reviewer";
      $this->registry->Assortments->save($body, $authenticated['user_id'], $companyId, $imgPath);
      $data = ["ok" => "Uploaded successfully"];

      $data = is_array($data) ? $data : [$data];
      return $this->jsonOk($data);

    }else {
      return $this->jsonErrorApplication(["error" => "missing usr type"]);
    }

  }

  public function getJson($uniqueId, rest_IRequest $request) {
    $this->registry->Cloud->uploadFile();
    return $this->jsonOk(["Error" => "Not implemented"]);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}
