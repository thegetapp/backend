<?php

class resource_v1_Collection extends resource_Base {

  public function postJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from postJson']);
  }

  public function getJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from getJson']);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}