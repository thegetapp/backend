<?php

class resource_v1_Companies extends resource_Base {

  public function postJson($uniqueId, rest_IRequest $request) {
    $body = json_decode($request->getBody());

    $validated = $this->registry->ValCompanies->validateRequest($body);
    if(!$validated){
      return $this->jsonErrorApplication(['error' => "request invalid." ]);
    }

    $authenticated = $this->registry->Auth->authenticateUser($body);

    if(!$authenticated) {
      error_log("Could not authenticate user !");
      return  $this->jsonErrorApplication(["error" => "Could not create company profile"]);
    }

     // Check if a reviewer have already created the company
    $companyId = $this->registry->Companies->getCompanyByGooglePlaceId($body->google_place_id);
    
    if($companyId['id']) {

      // Check if the company was officially created
      $companyUser = $this->registry->Users->getCompanyUserForCompany($companyId['id']);
      if(!$companyUser['user_id'] || $companyUser['user_id'] == $authenticatd['user_id']) {
        $data = $this->registry->Companies->updateExistingCompany($body, $authenticated['user_id']);
      }

    }else {
      $data = $this->registry->Companies->save($body, $authenticated['user_id']);
    }

    return $this->jsonOk(is_array($data) ? $data : [$data]);
  }

  public function getJson($uniqueId, rest_IRequest $request) {
    if($request->getPathParam()) {
      $data = $this->registry->Companies->fetchAllInfo($request->getPathParam());
    }
    return $this->jsonOk(is_array($data) ? $data : [$data]);
  }

  public function putJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from putJson']);
  }

  public function deleteJson($uniqueId, rest_IRequest $request) {
    return $this->jsonOk(['Hello from deleteJson']);
  }

}
