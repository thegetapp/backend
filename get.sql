CREATE TABLE user (
	id bigint PRIMARY KEY AUTO_INCREMENT,
	facebook_id bigint DEFAULT NULL,
	firstname varchar(255),
	lastname varchar(255),
	bithdate date,
	email varchar(255),
	password_hash varchar(512),
	salt varchar(512)
);

CREATE TABLE user_access (
  user_id bigint PRIMARY KEY,
  token varchar(255),
  fb_token varchar(255),
  date datetime,
  valid tinyint(1),
  FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE company (
	id bigint PRIMARY KEY AUTO_INCREMENT,
	facebook_id bigint default null,
	user_id bigint NOT NULL,
  google_place_id varchar(255) NOT NULL,
	street varchar(255),
  country varchar(100),
  city varchar(255),
  zip  int,
	phone varchar(30),
	website varchar(255),
	title varchar(255),
  description varchar(1000),
  lng float(10,6) NOT NULL,
  lat float(10,6) NOT NULL,
  logo_path varchar(500),
  created_dtm datetime,
  FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE company_user(
  company_id bigint,
  user_id bigint,
  FOREIGN KEY (company_id) REFERENCES company(id),
  FOREIGN KEY (user_id) REFERENCES user(id),
  PRIMARY KEY (user_id, company_id)
);

CREATE TABLE company_links (
	company_id bigint,
	network varchar(25),
	path varchar(255),
	FOREIGN KEY (company_id) REFERENCES company(id)
);

CREATE TABLE assortment (
	id int PRIMARY KEY AUTO_INCREMENT,
  title varchar(255),
	company_id bigint NOT NULL,
	user_id bigint NOT NULL,
	price int NOT NULL,
	description varchar(255),
	img_path varchar(255),
	FOREIGN KEY (company_id) REFERENCES company(id),
	FOREIGN KEY (user_id) REFERENCES user(id),
  lng float(10,6) NOT NULL,
  lat float(10,6) NOT NULL,
  user_type varchar(30),
  created_dtm datetime,
  is_valid tinyint DEFAULT 1,
  visible tinyint DEFAULT 1
);

CREATE TABLE opening_hours(
	id int PRIMARY KEY AUTO_INCREMENT,
	company_id bigint,
	day_of_week varchar(8) NOT NULL,
	open TIME,
	close TIME,
	FOREIGN KEY (company_id) REFERENCES company(id)
);

CREATE TABLE web_admin_user (
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(255),
  username varchar(255),
  password varchar(255)
);

INSERT INTO web_admin_user(name, username, password) VALUES ("Steffen Christensen", "stc", "qeVr@FtAK&J2");
INSERT INTO web_admin_user(name, username, password) VALUES ("Christian Ritter", "chr", "1e3cf6321a8f62bc5975585bf6e4c11a");

INSERT INTO user(firstname, lastname, bithdate, email, password_hash, salt) values(
  "Christ",
  "Macdonald",
  CURDATE(),
  "chris@macdonald.com",
  "1234",
  "123"
);

set @last_id = LAST_INSERT_ID();

INSERT INTO company(user_id, street, city, country, zip, phone, website, title, lng, lat, description) VALUES (
  1,
  "Strandgade 12",
  "København",
  "Danmark",
  1155,
  32963297,
  "http://hygge.dk",
  "Hygge Kroen",
  55.677903,
  12.5941113,
  "Bromølle Kro er omgivet af skov og idyllisk beliggende ved Åmose Å, hvor der er rig mulighed for vandreture og for at følge årstidernes skiften på tætteste hold. Bromølle Kro har ligget på dette smukke sted siden 1198- midt i Vestsjællands naturpark."
);

set @last_id = LAST_INSERT_ID();
set @user_id = 1;

INSERT INTO assortment(title, company_id, user_id, price, description, img_path) VALUES(
  "Lobster, onion and lavender",
  @last_id,
  @user_id,
  65,
  "Svinemørbrad pudses ren for sener og skæres i skiver på 3 cm. og bankes let.
Herefter steges skiverne i en gryde med margarine og karry (ca. 1 spsk stærk karry. eller efter smag) - salt og peber efter behov på mørbradbøfferne,",
  "https://charlotteagenda-charlotteagenda.netdna-ssl.com/wp-content/uploads/2015/11/bbq-plate.jpg"
);

INSERT INTO company(user_id, street, city, country, zip, phone, website, title, lng, lat, description) VALUES (
  @user_id,
  "Strandgade 12",
  "København",
  "Danmark",
  1155,
  32963297,
  "http://hygge.dk",
  "Hygge Kroen",
  55.677903,
  12.5941113,
  "Bromølle Kro er omgivet af skov og idyllisk beliggende ved Åmose Å, hvor der er rig mulighed for vandreture og for at følge årstidernes skiften på tætteste hold. Bromølle Kro har ligget på dette smukke sted siden 1198- midt i Vestsjællands naturpark."
);


INSERT INTO assortment(title, company_id, user_id, price, description, img_path) VALUES(
  "Flæske tæske godt tilbud!",
  @last_id,
  @user_id,
  0,
  125,
  "Stegt flæsk med persillesovs er en kendt dansk middagsret bestående af ovn- eller pandestegte skiver flæsk, der serveres med kogte kartofler og persillesovs. Der kan anvendes kød fra stegestykket eller kogestykket.",
  "http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/5151/4_880.jpg"
);

INSERT INTO assortment(title, company_id, user_id, price, description, img_path) VALUES(
  "Flæske tæske godt tilbud!",
  @last_id,
  @user_id,
  125,
  "Stegt flæsk med persillesovs er en kendt dansk middagsret bestående af ovn- eller pandestegte skiver flæsk, der serveres med kogte kartofler og persillesovs. Der kan anvendes kød fra stegestykket eller kogestykket.",
  "http://www.dk-kogebogen.dk/billeder-opskrifter/billeder/5151/4_880.jpg"
);

INSERT INTO company(user_id, street, city, country, zip, phone, website, title, lng, lat, description) VALUES (
  @user_id,
  "Sluseholmen 34",
  "København SV",
  "Danmark",
  2450,
  32963297,
  "http://SukuriSushi.dk",
  "Sukuri Sushi",
  55.677903,
  12.5941113,
  "Bromølle Kro er omgivet af skov og idyllisk beliggende ved Åmose Å, hvor der er rig mulighed for vandreture og for at følge årstidernes skiften på tætteste hold. Bromølle Kro har ligget på dette smukke sted siden 1198- midt i Vestsjællands naturpark."
);

set @last_id = LAST_INSERT_ID();

INSERT INTO assortment(title, company_id, user_id, price, description, img_path) VALUES(
  "Laks od",
  @last_id,
  @user_id,
  85,
  "Nigiri laks, laks new york, soyagravad laks og flammegrillet laks. Hosomaki inside-out med laks, agurk, miso aïoli & hvidløg daikon med laks",
  "https://roomservice2.s3.amazonaws.com/Product/55492/U8HyFfEx1UOMiyugE8QaRA/SquareHighRes.png"
);

INSERT INTO assortment(title, company_id, user_id, price, description, img_path) VALUES(
  "Mini maki maki",
  @last_id,
  @user_id,
  240,
  "Kaburimaki: hell's kitchen, ebi panko, shake aioli & lakse ceviche. Soya, wasabi og ingefær. (Bemærk: billede fra maki maki)",
  "https://roomservice2.s3.amazonaws.com/Product/55492/U8HyFfEx1UOMiyugE8QaRA/SquareHighRes.png"
);

INSERT INTO company(user_id, street, city, country, zip, phone, website, title, lat, lng, description) VALUES (
  @user_id,
  "Fjordgade 32",
  "Aalborg",
  "Danmark",
  9000,
  32963297,
  "http://aalborg.dk",
  "Aalborg Sushi",
  57.048820,
  9.921747,
  "Bromølle Kro er omgivet af skov og idyllisk beliggende ved Åmose Å, hvor der er rig mulighed for vandreture og for at følge årstidernes skiften på tætteste hold. Bromølle Kro har ligget på dette smukke sted siden 1198- midt i Vestsjællands naturpark."
);

set @last_id = LAST_INSERT_ID();

INSERT INTO assortment(title, company_id, user_id, price, description, img_path) VALUES(
  "Mini maki maki",
  @last_id,
  @user_id,
  240,
  "Jeg havde valgt at fejre min fødselsdag på Restaurant Green Solution House - bedste beslutning i lang tid!",
  "https://media-cdn.tripadvisor.com/media/photo-s/07/fa/b6/92/lokal-oko-gris.jpg"
);

set @last_id = null;
