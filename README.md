# php-rest-api
Simple REST api for PHP

## Getting startet
Clone repository and create a virtual host in apache2. Using this template

```
<VirtualHost *:443>
  DocumentRoot path-to-cloned-repository/www
  <Directory />
    Options FollowSymLinks
    AllowOverride None
  </Directory>
  <Directory path-to-cloned-repository/www>
    Options FollowSymLinks MultiViews
    AllowOverride all
    Require all granted
  </Directory>

  LogLevel warn
  ErrorLog ${APACHE_LOG_DIR}/api-ssl-error.log

  LogFormat "%{%FT%TZ}t %{pid}P %{tid}P %{UNIQUE_ID}n %a %I \"%{Accept}i\" %U %m %>s \"%{Content-Type}o\" %B %O %D %{PHP_PROCESS_TIME}n\"" rested
  CustomLog ${APACHE_LOG_DIR}/api-ssl-access.log rested

  SSLEngine On
  SSLProtocol All -SSLv2 -SSLv3
  SSLCertificateFile fullpath-to-your-pem
  SSLCertificateKeyFile fullpath-to-your-privatekey-pem

  BrowserMatch "MSIE [2-6]" nokeepalive ssl-unclean-shutdown downgrade-1.0 force-response-1.0
  BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown

  Redirect 404 /favicon.ico
  <Location /favicon.ico>
    ErrorDocument 404 "No favicon"
  </Location>

</VirtualHost>
```