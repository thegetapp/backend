<?php
set_include_path(
  get_include_path() . PATH_SEPARATOR . realpath(dirname(__FILE__) ."/../")
  . PATH_SEPARATOR . realpath(dirname(__FILE__) ."/../lib/")
);

require_once('../lib/rest.php');

// Defined routes for this rest api.
$routes = [];

$routes[] = new rest_Route(['regex' 	=> '~^/?$~',    
							'classname' => 'resource_v1_Collection',    
							'route' 	=> '/webadmin']);

$routes[] = new rest_Route(['regex' 	=> '~^/v1/collection/?$~',    
							'classname' => 'resource_v1_Collection',    
							'route' 	=> '/v1/collection']);

$routes[] = new rest_Route(['regex' 	=> '~^/v1/assortments/?$~',   
							'classname' => 'resource_v1_Assortments',   
							'route' 	  => '/v1/assortments']);

$routes[] = new rest_Route(['regex' 	=> '~^/v1/highlights/?$~',   
							'classname' => 'resource_v1_assortments_Highlights',   
							'route' 	  => '/v1/highlights']);

$routes[] = new rest_Route(['regex' 	=> '~^/v1/assortments/user?$~',   
							'classname' => 'resource_v1_assortments_User',   
							'route' 	=> '/v1/assortments/user']);

$routes[] = new rest_Route(['regex' 	=> '~^/v1/users/?$~',         
							'classname' => 'resource_v1_Users',         
							'route' 	=> '/v1/users']);

$routes[] = new rest_Route(['regex' 	=> '~^/v1/companies/?~',     
						    'classname' => 'resource_v1_Companies',      
						    'route' 	=> '/v1/companies']);

$routes[] = new rest_Route(['regex'     => '~^/v1/upload/?$~',        
							'classname' => 'resource_v1_assortments_Upload',        
							'route' 	=> '/v1/upload']);

$router  = new rest_Router($routes);
$gateway = new rest_Gateway($router);

$app = new Rest();
$app->run($gateway);
exit(0);
