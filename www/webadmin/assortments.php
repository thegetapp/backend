<?PHP
require_once("./include/membersite_config.php");

if(!$webadmin->CheckLogin())
{
  $webadmin->RedirectToURL("login.php");
  exit;
}

if( isset($_POST['deactivate']) ) {
  $webadmin->deactivateAssortment($_POST['deactivate']);
}
if( isset($_POST['activate']) ) {
  $webadmin->activateAssortment($_POST['activate']);
}
if( isset($_POST['delete']) ) {
  $webadmin->deleteAssortment($_POST['delete']);
}


?>

<html lang="en-gb" dir="ltr">

<head>
  <link rel="stylesheet" type="text/css" href="asset/css/uikit.gradient.min.css">
  <link rel="stylesheet" type="text/css" href="asset/css/uikit.rtl.min.css">
  <link rel="stylesheet" type="text/css" href="asset/css/card.css">
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="asset/js/uikit.min.js"></script>
</head>

<body>

<div class="uk-container uk-container-center uk-margin-top uk-margin-large-bottom">

  <nav class="uk-navbar uk-navbar-custom uk-margin-large-bottom">
    <a class="uk-navbar-brand" href="main.php"><?= $webadmin->UserFullName(); ?></a>
    <ul class="uk-navbar-nav uk-hidden-small">
      <li class="uk-active">
        <a href="assortments.php">Assortments</a>
      </li>
      <li>
        <a href="companies.php">Companies</a>
      </li>
      <li>
        <a target="_blank" href="https://nodequery.com/login">Server stats</a>
      </li>
    </ul>
  </nav>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-1-1">
      <h1 class="uk-heading-large">Assortments</h1>
      <p class="uk-text-large">Få et overblik over alle tilgængelige assortments</p>
      <h2>
        Vi har pt <?php echo $webadmin->countAssortments()[0] ?> opslag </br>
        <?php echo $webadmin->countCompanies()[0] ?> virksomheder </br>
        hvoraf <?php echo $webadmin->countReviewerPosts()[0] ?> opslag er fra reviewers! </br>
        og <?php echo $webadmin->countCompanyPosts()[0] ?> opslag er fra virksomheder!
      </h2>
    </div>
  </div>


  <div class="uk-grid" data-uk-grid-margin>
    <?php foreach($webadmin->fetchAssortments() as $key){ ?>
      <div class="uk-width-1-4">
        <div class="card">
          <img src=<?php echo $key['img_path'] ?> alt="Avatar" style="width:100%">


          <div class="container">


            <h2><b><?php echo $key['title_company'] ?></b></h2>
            <h3><b><?php echo $key['title_food'] ?></b></h3>
            <p><?php echo $key['description'] ?></p>

            <h5>
              <b>Pris:</b>
              <?php echo $key['price'] ?>
            </h5>

            <hr/>

            <h6>
              <b>id:</b>
              <?php echo $key['aid'] ?> <b>Uploaded:</b> <?php echo $key['upload_dtm'] ?>
              <br/>
              <b>Uploaded af:</b>
              <?php echo $key['firstname'] ?> <?php echo $key['lastname'] ?> <?php echo $key['email'] ?>
              <br/>
              <b>Aktiv:</b>
              <?php echo $key['is_valid'] ?>
              <br/>
              <b>usrType:</b>
              <?php echo $key['user_type'] ?>
            </h6>


            <form method="POST" action="assortments.php">
              <input name="deactivate" id="deactivate" type="hidden" value=<?php echo $key['aid'] ?> />
              <button type="submit" value="deactivate" class="uk-button-primary uk-width-1-1 uk-margin-small-bottom">Deaktivér</button>
            </form>

            <form method="POST" action="assortments.php">
              <input name="activate" id="activate" type="hidden" value=<?php echo $key['aid'] ?> />
              <button type="submit" value="activate" class="uk-button-success uk-width-1-1 uk-margin-small-bottom">Aktivér</button>
            </form>

            <form method="POST" action="assortments.php">
              <input name="delete" id="delete" type="hidden" value=<?php echo $key['aid'] ?> />
              <button type="submit" value="delete" class="uk-button-danger uk-width-1-1 uk-margin-small-bottom">SLET</button>
            </form>



          </div>
        </div>
      </div>


    <?php } ?>
  </div>



</body>
</html>