<?PHP
/*
    Registration/Login script from HTML Form Guide
    V1.0

    This program is free software published under the
    terms of the GNU Lesser General Public License.
    http://www.gnu.org/copyleft/lesser.html


This program is distributed in the hope that it will
be useful - WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.

For updates, please visit:
http://www.html-form-guide.com/php-form/php-registration-form.html
http://www.html-form-guide.com/php-form/php-login-form.html

*/
class Webadmin
{
  private $servername, $username, $password, $dbname, $rand_key;
  public $conn;

  //-----Initialization -------
  function __construct(){
    $this->servername = "46.101.123.234";
    $this->username   = "getservice";
    $this->password   = "y5HvfxaD";
    $this->dbname     = "getdev";

    // Create connection

    try {
      $this->conn = new PDO("mysql:host={$this->servername};port=3306;dbname=getdev;", $this->username, $this->password);
      // set the PDO error mode to exception
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
      error_log($e);
      echo "Connection failed: " . $e->getMessage();
    }
  }

  function InitDB($host,$uname,$pwd,$database,$tablename)
  {
    $this->db_host  = $host;
    $this->username = $uname;
    $this->pwd  = $pwd;
    $this->database  = $database;
    $this->tablename = $tablename;

  }

  function SetRandomKey($key)
  {
    $this->rand_key = $key;
  }

  function Login()
  {
    if(empty($_POST['username']))
    {
      $this->HandleError("UserName is empty!");
      return false;
    }

    if(empty($_POST['password']))
    {
      $this->HandleError("Password is empty!");
      return false;
    }

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    if(!isset($_SESSION)){ session_start(); }
    if(!$this->CheckLoginInDB($username,$password))
    {
      return false;
    }

    $_SESSION[$this->GetLoginSessionVar()] = $username;

    return true;
  }

  function CheckLogin()
  {
    if(!isset($_SESSION)){ session_start(); }

    $sessionvar = $this->GetLoginSessionVar();

    if(empty($_SESSION[$sessionvar]))
    {
      return false;
    }
    return true;
  }

  //-------Public Helper functions -------------

  function RedirectToURL($url)
  {
    header("Location: $url");
    exit;
  }

  //-------Private Helper functions-----------

  function GetLoginSessionVar()
  {
    $retvar = md5($this->rand_key);
    $retvar = 'usr_'.substr($retvar,0,10);
    return $retvar;
  }

  function CheckLoginInDB($username,$password) {
    if(!$this->DBLogin())
    {
      $this->HandleError("Database login failed!");
      return false;
    }

    $pwdmd5   = md5($password);
    $stmt     = "Select name from web_admin_user where username=:username and password=:password";
    $values   = [":username" => $username, "password" => $pwdmd5];

    $statement = $this->conn->prepare($stmt);
    $statement->execute($values);

    $result = $statement->fetch(PDO::FETCH_ASSOC);

    if(isset($result['name'])) {
      $_SESSION['name_of_user'] = $result['name'];
      return true;
    }

    return false;
  }

  function DBLogin() {
    // Create connection

    try {
      $this->conn = new PDO("mysql:host={$this->servername};port=3306;dbname=getdev", $this->username, $this->password);
      // set the PDO error mode to exception
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e) {
      error_log($e);
    }

    return true;
  }

  function UserFullName() {
    return isset($_SESSION['name_of_user'])?$_SESSION['name_of_user']:'';
  }

  function countAssortments(){
    $stmt = "SELECT count(*) from assortment";
    $statement = $this->conn->prepare($stmt);
    $statement->execute([]);

    $result = $statement->fetch();
    $_SESSION['assortments_count'] = $result;

    return $result;
  }

  function countCompanies(){
    $stmt = "SELECT count(*) from company";
    $statement = $this->conn->prepare($stmt);
    $statement->execute([]);

    $result = $statement->fetch();
    $_SESSION['companies_count'] = $result;

    return $result;
  }

  function countReviewerPosts(){
    $stmt = "SELECT count(*) FROM assortment where user_type = 'reviewer'";
    $statement = $this->conn->prepare($stmt);
    $statement->execute([]);

    $result = $statement->fetch();
    $_SESSION['reviewer_post_count'] = $result;

    return $result;
  }

  function countCompanyPosts(){
    $stmt = "SELECT count(*) FROM assortment where user_type = 'company'";
    $statement = $this->conn->prepare($stmt);
    $statement->execute([]);

    $result = $statement->fetch();
    $_SESSION['company_post_count'] = $result;

    return $result;
  }

  function deactivateAssortment($id){
    $stmt = "UPDATE assortment set is_valid = 0 WHERE id = :assortment_id";
    $values = [":assortment_id" => $id];

    $statement = $this->conn->prepare($stmt);
    $statement->execute($values);
  }

  function activateAssortment($id){
    $stmt = "UPDATE assortment set is_valid = 1 WHERE id = :assortment_id";
    $values = [":assortment_id" => $id];

    $statement = $this->conn->prepare($stmt);
    $statement->execute($values);
  }

  function deleteAssortment($id){
    $stmt = "DELETE FROM assortment WHERE id = :assortment_id";
    $values = [":assortment_id" => $id];

    $statement = $this->conn->prepare($stmt);
    $statement->execute($values);
  }

  function fetchCompanies(){
    $stmt = "SELECT * from company order by 1 DESC";

    $statement = $this->conn->prepare($stmt);
    $statement->execute([]);

    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    $_SESSION['companies'] = $result;

    return $result;
  }

  function fetchAssortments() {
    $offset   = 0;
    $lat      = "55.676097";
    $long     = "12.568337";
    $distance = 15;
    $order    = "assortment.created_dtm";
    if(isset($_POST['offset'])) {
      $offset = $_POST['offset'];
    }
    if(isset($_POST['lat'])) {
      $lat = $_POST['lat'];
    }
    if(isset($_POST['long'])) {
      $long = $_POST['long'];
    }
    if(isset($_POST['distance'])) {
      $distance = $_POST['distance'];
    }
    if(isset($_POST['order'])) {
      $order = $_POST['order'];
    }


    $stmt = "
      SELECT
             a.description,
             a.title as title_food,
             a.id as aid,
             a.created_dtm as upload_dtm,
             a.img_path,
             a.is_valid,
             a.price,
             a.user_type,
             u.firstname,
             u.lastname,
             u.email,
             c.title as title_company,
             REPLACE(price, '.', ',') as price,
             CONCAT('https://graph.facebook.com/v2.8/', c.facebook_id, '/picture') as logo_path
        FROM assortment a
        JOIN company c
          ON a.company_id = c.id
        JOIN user u
          ON a.user_id = u.id
    ORDER BY aid DESC
       LIMIT 100
       OFFSET {$offset}";

    $values = [":order" => $order];

    $statement = $this->conn->prepare($stmt);
    $statement->execute($values);

    $result = $statement->fetchAll(PDO::FETCH_ASSOC);
    $_SESSION['assortments'] = $result;

    return $result;
  }
}
