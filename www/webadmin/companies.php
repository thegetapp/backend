<?PHP
require_once("./include/membersite_config.php");

if(!$webadmin->CheckLogin())
{
  $webadmin->RedirectToURL("login.php");
  exit;
}

if( isset($_POST['deactivate']) ) {
  $webadmin->deactivateAssortment($_POST['deactivate']);
}
if( isset($_POST['activate']) ) {
  $webadmin->activateAssortment($_POST['activate']);
}


?>

<html lang="en-gb" dir="ltr">

<head>
  <link rel="stylesheet" type="text/css" href="asset/css/uikit.gradient.min.css">
  <link rel="stylesheet" type="text/css" href="asset/css/uikit.rtl.min.css">
  <link rel="stylesheet" type="text/css" href="asset/css/card.css">
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="asset/js/uikit.min.js"></script>
</head>

<body>

<div class="uk-container uk-container-center uk-margin-top uk-margin-large-bottom">

  <nav class="uk-navbar uk-navbar-custom uk-margin-large-bottom">
    <a class="uk-navbar-brand" href="main.php"><?= $webadmin->UserFullName(); ?></a>
    <ul class="uk-navbar-nav uk-hidden-small">
      <li>
        <a href="assortments.php">Assortments</a>
      </li>
      <li class="uk-active">
        <a href="companies.php">Companies</a>
      </li>
      <li>
        <a target="_blank" href="https://nodequery.com/login">Server stats</a>
      </li>
    </ul>
  </nav>

  <div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-1-1">
      <h1 class="uk-heading-large">Companies</h1>
      <p class="uk-text-large">Få et overblik over alle oprettede virksomheder</p>
      <h2>
        Vi har pt <?php echo $webadmin->countAssortments()[0] ?> opslag </br>
        <?php echo $webadmin->countCompanies()[0] ?> virksomheder </br>
        hvoraf <?php echo $webadmin->countReviewerPosts()[0] ?> opslag er fra reviewers! </br>
        og <?php echo $webadmin->countCompanyPosts()[0] ?> opslag er fra virksomheder!
      </h2>
    </div>
  </div>


  <div class="uk-overflow-container">
  <table class="uk-table uk-table-condensed uk-table-hover uk-table-striped uk-overflow-container">
    <thead>
    <tr>
      <th>id</th>
      <th>title</th>
      <th>zip</th>
      <th>city</th>
      <th>street</th>
      <th>phone</th>
      <th>website</th>
      <th>oprettet</th>
      <th>description</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($webadmin->fetchCompanies() as $key){ ?>
    <tr>
      <td>
        <?php echo $key['id'] ?>
      </td>
      <td>
        <?php echo $key['title'] ?>
      </td>
      <td>
        <?php echo $key['zip'] ?>
      </td>
      <td>
        <?php echo $key['city'] ?>
      </td>
      <td class="uk-width-1-1">
        <?php echo $key['street'] ?>
      </td>
      <td>
        <?php echo $key['phone'] ?>
      </td>
      <td>
        <?php echo $key['website'] ?>
      </td>
      <td>
        <?php echo $key['created_dtm'] ?>
      </td>
      <td>
        <?php echo $key['description'] ?>
      </td>

    </tr>
    <?php } ?>

    </tbody>
  </table>
</div>
  </div>



</body>
</html>