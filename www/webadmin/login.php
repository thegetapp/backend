<?PHP
require_once("./include/membersite_config.php");

if(isset($_POST['submitted'])) {
    if($webadmin->Login()) {
      $webadmin->RedirectToURL("assortments.php");
    }else {
      $webadmin->RedirectToURL("login.php");
    }
}

?>


<html lang="en-gb" dir="ltr" class="uk-height-1-1">

<head>
    <link rel="stylesheet" type="text/css" href="asset/css/card.css">
    <link rel="stylesheet" type="text/css" href="asset/css/uikit.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/uikit2.min.css">
    <link rel="stylesheet" type="text/css" href="asset/css/uikit.gradient.min.css">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="asset/js/uikit.min.js"></script>
</head>

<body class="uk-height-1-1 animate">

  <div class="uk-vertical-align uk-text-center uk-height-1-1">
    <div class="uk-vertical-align-middle" style="width: 250px;">

      <img class="uk-margin-bottom" width="1200px" src="asset/imgs/logo.png" alt="">

      <form id="login" action="login.php" method="post"  class="uk-panel uk-panel-box uk-form">
        <input type='hidden' name='submitted' id='submitted' value='1'/>
        <div class="uk-form-row">
            <input class="uk-width-1-1 uk-form-large" name="username" type="text" placeholder="Username">
        </div>
        <div class="uk-form-row">
            <input class="uk-width-1-1 uk-form-large" name="password" type="password" placeholder="Password">
        </div>
        <div class="uk-form-row">
            <input type="submit" value="Login" style="color:'#fff'" class="uk-width-1-1 uk-button uk-button-primary uk-button-large"/>
        </div>
      </form>
    </div>
  </div>

</body>

</html>